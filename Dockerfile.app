###### Install KiCad #####
FROM ubuntu:noble AS python-kicad

SHELL ["/bin/bash", "-c"]

ENV TZ=Etc/UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get -yqq update && \
    apt-get -yqq install apt-utils software-properties-common && \ 
    add-apt-repository --yes ppa:kicad/kicad-8.0-releases && \
    apt-get -yqq update && \
    apt-get -yqq install --no-install-recommends kicad;

###### Install Python dependencies #####
FROM python-kicad AS builder
RUN apt-get -yqq install python3-poetry;

ENV POETRY_HOME=/opt/poetry
ENV POETRY_NO_INTERACTION=1
ENV POETRY_VIRTUALENVS_IN_PROJECT=1
ENV POETRY_VIRTUALENVS_OPTIONS_ALWAYS_COPY=1
ENV POETRY_VIRTUALENVS_CREATE=1
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV POETRY_CACHE_DIR=/opt/.cache

ARG PYTHON_DIST_PACKAGES=/usr/lib/python3/dist-packages
ARG VENV_SITE_PACKAGES=/app/.venv/lib/python3.12/site-packages

WORKDIR /app
COPY pyproject.toml poetry.lock ./

RUN poetry install --without dev --no-root && rm -rf $POETRY_CACHE_DIR
RUN cp ${PYTHON_DIST_PACKAGES}/{pcbnew.py,_pcbnew.so} ${VENV_SITE_PACKAGES}

###### Runtime dependencies #####
FROM python-kicad AS server

ENV PATH="/app/.venv/bin:$PATH"

ARG VIRTUAL_ENV=/app/.venv

WORKDIR /app

RUN apt-get -yqq install git libvips
COPY --from=builder ${VIRTUAL_ENV} ${VIRTUAL_ENV}
COPY app /app/app
COPY alembic.ini /app/alembic.ini

CMD ["which", "python"]
