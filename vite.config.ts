import { configDefaults, defineConfig } from 'vitest/config';
import { purgeCss } from 'vite-plugin-tailwind-purgecss';
import { sveltekit } from '@sveltejs/kit/vite';

export default defineConfig({
	plugins: [sveltekit(), purgeCss()],
	define: {
		// Eliminate in-source test code
		'import.meta.vitest': 'undefined'
	},
	test: {
		// jest like globals
		globals: true,
		environment: 'jsdom',
		// in-source testing
		includeSource: ['src/**/*.{js,ts,svelte}'],
		// Add @testing-library/jest-dom matchers & mocks of SvelteKit modules
		setupFiles: ['./setupTest.ts'],
		reporters: ['default', 'junit'],
		outputFile: {
			junit: './frontend.xml'
		},
		// Exclude files in c8
		coverage: {
			provider: 'istanbul',
			reportsDirectory: '.coverage',
			exclude: ['setupTest.ts'],
			reporter: ['cobertura', 'html',]
		},
		// Exclude playwright tests folder
		exclude: [...configDefaults.exclude, 'src/tests/integration/*.test.ts']
	}
});
