import adapter from '@sveltejs/adapter-node';
import { vitePreprocess } from '@sveltejs/vite-plugin-svelte';

const HOST = new URL(process.env.VITE_FRONTEND_URL ?? 'https://edea.test').host;

/** @type {import('@sveltejs/kit').Config} */
const config = {
	preprocess: vitePreprocess(),
	vitePlugin: {
		inspector: true,
	},
	kit: {
		adapter: adapter(),
		alias: {
			$client: './src/client',
			$ENV: './src/ENV',
			$lib: './src/lib',
		},
		csp: {
			mode: 'auto',
			directives: {
				'script-src': [
					'self',
					// for the consent banner
					`offen.${HOST}`,
					// for the svgpanzoom library
					"'sha256-fIhp3eOSbYIMFnWob0qnUBRP1Xhl8vIczTpK8jbqDVA='",
				],
				'object-src': ['none'],
				'base-uri': ['self'],
				'frame-src': ["'self'", `offen.${HOST}`],
				'frame-ancestors': ["'self'", `offen.${HOST}`],
				'upgrade-insecure-requests': true,
			},
		},
	},
};
export default config;
