import random
from typing import Any
from typing import Generator as Gener

import pytest
import pytest_mock
import yaml
from httpx import AsyncClient, get


@pytest.fixture(scope="module")
def rules_from_repo_edea_yaml() -> Gener[dict[str, Any], None, None]:
    """Get the edea.yaml file from the repo."""
    content = get(
        "https://gitlab.com/edea-dev/test-rules/-/raw/main/edea.yml?ref_type=heads"
    ).content
    yield yaml.safe_load(content)["rules"]


@pytest.mark.anyio
async def test_repo_without_edea_yml_file(client: AsyncClient) -> None:
    response = await client.post(
        "/submit",
        json={
            "repo_url": "https://gitlab.com/edea-dev/edea-ps",
        },
        headers={"Webauth-User": "new_rules_user"},
    )

    assert response.status_code == 422


@pytest.mark.anyio
async def test_new_rules_from_unsupported_git_host(client: AsyncClient) -> None:
    response = await client.post(
        "/submit",
        json={
            "repo_url": "https://example.com/edea-dev/edea-ps",
        },
        headers={"Webauth-User": "new_rules_user"},
    )

    assert response.status_code == 422


@pytest.mark.anyio
async def test_new_rules_from_non_git_repo_url(client: AsyncClient) -> None:
    response = await client.post(
        "/submit",
        json={
            "repo_url": "https://gitlab.com/help/",
        },
        headers={"Webauth-User": "new_rules_user"},
    )
    assert response.status_code == 422
    assert (
        response.json()["detail"]
        == "failed to clone repository at https://gitlab.com/help/"
    )


@pytest.mark.anyio
async def test_new_rules_from_valid_host_but_invalid_path(client: AsyncClient) -> None:
    response = await client.post(
        "/submit",
        json={
            "repo_url": "https://gitlab.com",
        },
        headers={"Webauth-User": "new_rules_user"},
    )
    assert response.status_code == 422


@pytest.mark.anyio
async def test_repo_with_with_missing_rules_key_in_edea_yml(
    client: AsyncClient, mocker: pytest_mock.MockFixture
) -> None:
    with mocker.patch(
        "app.services.repository.EDeAConfig.from_yaml",
        side_effect=yaml.YAMLError("invalid yaml"),
    ):
        response = await client.post(
            "/submit",
            json={
                "repo_url": "https://gitlab.com/edea-dev/malformed-test-modules",
            },
            headers={"Webauth-User": "new_rules_user"},
        )
    assert response.status_code == 422


@pytest.mark.anyio
async def test_create_correct_number_of_rules_from_repo(
    new_rules: list[dict[str, Any]],
    rules_from_repo_edea_yaml: dict[str, dict[str, Any]],
) -> None:
    """Test that the correct number of modules are created from the repo."""
    assert len(new_rules) == len(rules_from_repo_edea_yaml)


@pytest.mark.anyio
async def test_create_rules_with_right_description(
    new_rules: list[dict[str, Any]],
    rules_from_repo_edea_yaml: dict[str, dict[str, Any]],
) -> None:
    for m1, m2 in zip(new_rules, rules_from_repo_edea_yaml.values()):
        assert m1.get("description", None) == m2.get("description", None)


@pytest.mark.anyio
async def test_rules_full_text_search(
    client: AsyncClient, new_rules: list[dict[str, Any]]
) -> None:
    q = random.choice(new_rules)["description"]
    response = await client.get("/rules/search", params={"q": q})
    response.raise_for_status()

    assert response.json()[0]["description"] == q


@pytest.mark.anyio
async def test_get_rules_by_name(
    client: AsyncClient, new_rules: list[dict[str, Any]]
) -> None:
    rule_set = random.choice(new_rules)
    response = await client.get(f"/rules/new-rules-user/{rule_set['name']}")
    response.raise_for_status()

    assert response.json() == rule_set


@pytest.mark.anyio
async def test_get_non_existent_rules_by_name(
    client: AsyncClient, new_rules: list[dict[str, Any]]
) -> None:
    rule_set = random.choice(new_rules)
    response = await client.get(f"/rules/doesnt-exist/{rule_set['name']}")

    assert response.status_code == 404


@pytest.mark.anyio
async def test_get_non_existent_rules(
    client: AsyncClient, new_rules: list[dict[str, Any]]
) -> None:
    response = await client.get("/rules/someones/rules")
    assert response.status_code == 404


@pytest.mark.anyio
async def test_get_private_rules(
    client: AsyncClient,
) -> None:
    response = await client.post(
        "/submit",
        json={
            "repo_url": "https://gitlab.com/edea-dev/test-rules.git",
            "private": True,
        },
        headers={"Webauth-User": "private-rule-user"},
    )

    assert response.status_code == 201
    rules = response.json()
    r = rules[0]

    response = await client.get(f"/rules/{r['user']['displayname']}/{r['name']}")
    assert response.status_code == 404

    response = await client.get(
        f"/rules/{r['user']['displayname']}/{r['name']}",
        headers={"Webauth-User": "private-rule-user"},
    )

    assert response.status_code == 200
    assert response.json() == rules[0]


@pytest.mark.anyio
async def test_repository_unique_per_user(
    client: AsyncClient,
) -> None:
    response = await client.post(
        "/submit",
        json={
            "repo_url": "https://gitlab.com/edea-dev/test-rules",
            "private": True,
        },
        headers={"Webauth-User": "unique-rule-user"},
    )

    assert response.status_code == 201

    response = await client.post(
        "/submit",
        json={
            "repo_url": "https://gitlab.com/edea-dev/test-rules",
            "private": True,
        },
        headers={"Webauth-User": "unique-rule-user"},
    )

    assert response.status_code == 409


@pytest.mark.anyio
async def test_get_readme(client: AsyncClient, new_rules: list[dict[str, Any]]) -> None:
    r = random.choice(new_rules)
    response = await client.get(f"/rules/{r['user']['displayname']}/{r['name']}/readme")
    assert response.status_code == 200

    assert response.headers["content-type"] == "text/html; charset=utf-8"


@pytest.mark.anyio
async def test_delete_rules(
    client: AsyncClient, new_rules: list[dict[str, Any]]
) -> None:
    r = random.choice(new_rules)
    response = await client.delete(
        f"/rules/{r['user']['displayname']}/{r['name']}",
        headers={"Webauth-User": "new_rules_user"},
    )
    assert response.status_code == 204
    del new_rules[new_rules.index(r)]

    response = await client.get(f"/rules/{r['user']['displayname']}/{r['name']}")
    assert response.status_code == 404


@pytest.mark.anyio
async def test_del_non_existent_rules(
    client: AsyncClient,
) -> None:
    response = await client.delete(
        "/rules/someones/rules", headers={"Webauth-User": "new_rules_user"}
    )
    assert response.status_code == 404


@pytest.mark.anyio
async def test_rule_set_content(
    client: AsyncClient, new_rules: list[dict[str, Any]]
) -> None:
    r = random.choice(new_rules)
    response = await client.get(f"/rules/{r['user']['displayname']}/{r['name']}")
    assert (
        "# Matching JLCPCB capabilities: https://jlcpcb.com/capabilities/pcb-capabilities"
        in response.json()["body"]
    )
