import uuid

import pytest
from httpx import AsyncClient


@pytest.mark.anyio
async def test_get_current_user_from_x_webauth_header(client: AsyncClient) -> None:
    """Test that the current user can be retrieved from the Webauth-User header."""
    new_user = {
        "id": str(uuid.uuid4()),
        "subject": "asafdsgdg",
        "displayname": "user",
        "groups": ["group"],
        "roles": ["role"],
    }
    response = await client.post("/auth_middleware/new_user", json=new_user)
    assert response.status_code == 201

    response = await client.get(
        "/auth_middleware/current_user",
        headers={
            "Webauth-User": "user",
            "Webauth-Groups": "group",
            "Webauth-Roles": "role",
        },
    )

    assert response.json() == new_user
