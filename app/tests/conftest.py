import asyncio
import os
import shutil
from typing import Any
from typing import AsyncGenerator as AsyncGener
from typing import AsyncIterable
from unittest.mock import patch

import pytest
from httpx import AsyncClient

from app import app
from app.config import CONFIG
from app.db import fts, queries
from app.db.connection import Base, engine
from app.services import cache, tasks


@pytest.fixture(scope="session")
def anyio_backend() -> str:
    return "asyncio"


@pytest.fixture(scope="session")
def test_folder() -> str:
    return os.path.dirname(os.path.realpath(__file__))


@pytest.fixture(scope="session", autouse=True)
async def patch_taskiq_kiq() -> Any:
    with patch(
        "app.services.tasks.render_single_kicad_file.kiq",
        tasks.render_single_kicad_file,
    ), patch(
        "app.services.tasks.render_module_file.kiq",
        tasks.render_module_file,
    ), patch(
        "app.services.tasks.render_readme.kiq", tasks.render_readme
    ), patch(
        "app.services.tasks.generate_violations_report.kiq",
        tasks.generate_violations_report,
    ):
        yield


@pytest.fixture(scope="module")
def event_loop() -> Any:
    """
    Overrides pytest default function scoped event loop.
    This is needed to set up the DB once per session.
    """
    policy = asyncio.get_event_loop_policy()
    loop = policy.new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(autouse=True, scope="session")
async def setup_db() -> AsyncIterable[None]:
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)
        await queries.category.create_categories()
    await cache.setup()
    fts.create_fts()
    yield


@pytest.fixture(scope="session")
async def new_modules(client: AsyncClient) -> AsyncGener[list[dict[str, Any]], None]:
    """Create a new module for testing"""
    response = await client.post(
        "/submit",
        json={
            "repo_url": "https://gitlab.com/edea-dev/test-modules.git",
        },
        headers={"Webauth-User": "new-module-user"},
    )

    assert response.status_code == 201
    modules = response.json()
    assert len(modules) == 7
    yield modules


@pytest.fixture(scope="session")
async def new_rules(client: AsyncClient) -> AsyncGener[list[dict[str, Any]], None]:
    """Create new rules for testing"""
    response = await client.post(
        "/submit",
        json={
            "repo_url": "https://gitlab.com/edea-dev/test-rules",
        },
        headers={"Webauth-User": "new-rules-user"},
    )

    assert response.status_code == 201
    yield response.json()


@pytest.fixture(scope="session")
async def new_module_id(new_modules: list[dict[str, Any]]) -> AsyncGener[str, None]:
    """Get the id of the newly created module."""
    yield [module["id"] for module in new_modules if module["name"] == "LDO/HT7533-1"][
        0
    ]


@pytest.fixture(scope="session")
async def new_module_name(new_modules: list[dict[str, Any]]) -> AsyncGener[str, None]:
    """Get the id of the newly created module."""
    yield [
        f'{m["user"]["displayname"]}/{m["repository"]["name"]}/{m["name"]}'
        for m in new_modules
        if m["name"] == "LDO/HT7533-1"
    ][0]


@pytest.fixture(scope="session")
async def to_be_delete_module_name(
    new_modules: list[dict[str, Any]]
) -> AsyncGener[str, None]:
    """Get the id of the newly created module."""
    m = [module for module in new_modules if module["name"] == "3v3ldo"][0]
    new_modules.pop(new_modules.index(m))
    yield f'{m["user"]["displayname"]}/{m["repository"]["name"]}/{m["name"]}'


@pytest.fixture(scope="session")
async def client() -> AsyncIterable[AsyncClient]:
    async with AsyncClient(app=app, base_url="http://test/v1") as client:
        yield client


def pytest_sessionfinish(session, exitstatus):  # type: ignore
    """
    This hook is called after the entire session has been run.
    """
    _db_cleanup()
    _storage_cleanup()


def _db_cleanup() -> None:
    "delete the test database"
    db_file = __file__.replace("conftest.py", "test.sqlite")
    if os.path.exists(db_file):
        os.remove(db_file)


def _storage_cleanup() -> None:
    "delete the test storage folder"
    shutil.rmtree(CONFIG.storage_path.absolute(), ignore_errors=True)
