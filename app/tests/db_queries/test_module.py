from typing import Any, AsyncIterable

import pytest

from app.db.connection import AsyncSession, SessionLocal
from app.db.models.category import Category
from app.db.models.module import Module
from app.db.models.repository import Repository
from app.db.models.user import SUDO, User
from app.db.queries.module import get_module_by_name, search


@pytest.fixture(scope="module")
async def session(client: Any) -> AsyncIterable[AsyncSession]:
    session = SessionLocal()
    yield session
    await session.close()


DbTestObjects = tuple[list[Module], list[Module], User]


@pytest.fixture(scope="module")
async def objects(
    session: AsyncSession,
) -> AsyncIterable[DbTestObjects]:
    user = User(subject="sddsgege", displayname="pagination", groups=[], roles=[])
    category = Category(name="pagination", description="Description 1")
    session.add(user)
    session.add(category)
    await session.flush()

    repo = Repository(
        url="https://gitlab.com/edea-dev/example",
        user_id=user.id,
        type="git",
        location="/tmp",
        name="example",
    )
    session.add(repo)
    await session.flush()

    module_names = ["Module 1", "Module 2", "Module 3", "Module 4", "Module 5"]
    public_modules = [
        Module(name=name, repo_id=repo.id, user_id=user.id, category_id=category.id)
        for name in module_names
    ]
    session.add_all(public_modules)

    private_modules = [
        Module(
            name=f"Private {name}",
            private=True,
            repo_id=repo.id,
            user_id=user.id,
            category_id=category.id,
        )
        for name in module_names
    ]
    session.add_all(private_modules)

    await session.commit()

    yield public_modules, private_modules, user


@pytest.mark.anyio
async def test_admin_can_access_private_modules(
    session: AsyncSession, objects: DbTestObjects
) -> None:
    _, private_modules, _ = objects
    result, _ = await search(SUDO, "", None, 0, 100, 12345, session)
    modules = set(module for module in result if module.private is True)

    assert modules.issuperset(private_modules)


@pytest.mark.anyio
async def test_random_order(session: AsyncSession, objects: DbTestObjects) -> None:
    modules, _, user = objects
    result, _ = await search(user, "", None, 0, 5, 12345, session)

    assert isinstance(result, list)

    assert len(result) == 5

    # Assert that the order of the modules is different from the original order
    assert result != modules


@pytest.mark.anyio
async def test_negative_limit_or_page_number(
    session: AsyncSession, objects: DbTestObjects
) -> None:
    *_, user = objects
    result, _ = await search(user, "", None, -1, 5, 12345, session)

    assert isinstance(result, list)
    assert len(result) == 0

    result, _ = await search(user, "", None, 1, -1, 12345, session)

    assert isinstance(result, list)
    assert len(result) == 0


@pytest.mark.anyio
async def test_zero_limit(session: AsyncSession, objects: DbTestObjects) -> None:
    *_, user = objects
    result, _ = await search(user, "", None, 1, 0, 12345, session)

    assert isinstance(result, list)
    assert len(result) == 0


@pytest.mark.anyio
async def test_page_greater_than_available_modules(
    session: AsyncSession, objects: DbTestObjects
) -> None:
    *_, user = objects
    result, _ = await search(user, "", None, 100, 5, 12345, session)

    assert isinstance(result, list)
    assert len(result) == 0


@pytest.mark.anyio
async def test_full_text_search_with_empty_query(
    session: AsyncSession, objects: DbTestObjects
) -> None:
    *_, user = objects
    result, _ = await search(user, "", None, 100, 5, 12345, session)
    assert isinstance(result, list)
    assert len(result) == 0


@pytest.mark.anyio
async def test_get_module_by_name(
    session: AsyncSession, objects: DbTestObjects
) -> None:
    public_modules, _, user = objects
    result = await get_module_by_name(
        user.displayname,
        public_modules[0].repository.name,
        public_modules[0].name,
        user,
        session,
    )
    assert result == public_modules[0]
