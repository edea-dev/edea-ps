from typing import Any, AsyncIterable

import pytest

from app.db.connection import AsyncSession, SessionLocal
from app.db.models.repository import Repository
from app.db.models.rule_set import RuleSet
from app.db.models.user import SUDO, User
from app.db.queries.rule_set import full_text_search, get_rule_set_by_name, pagination


@pytest.fixture(scope="module")
async def session(client: Any) -> AsyncIterable[AsyncSession]:
    session = SessionLocal()
    yield session
    await session.close()


DbTestObjects = tuple[list[RuleSet], list[RuleSet], User]


@pytest.fixture(scope="module")
async def objects(
    session: AsyncSession,
) -> AsyncIterable[DbTestObjects]:
    user = User(subject="pagination2", displayname="Test User", groups=[], roles=[])
    session.add(user)
    await session.flush()

    repo = Repository(
        url="https://gitlab.com/edea-dev/example",
        user_id=user.id,
        type="git",
        location="/tmp",
        name="example",
    )
    session.add(repo)
    await session.flush()

    rules_names = ["Rule 1", "Rule 2", "Rule 3", "Rule 4", "Rule 5"]
    public_rules = [
        RuleSet(name=name, repo_id=repo.id, user_id=user.id, body="")
        for name in rules_names
    ]
    session.add_all(public_rules)

    private_rules = [
        RuleSet(
            name=f"Private {name}",
            private=True,
            repo_id=repo.id,
            user_id=user.id,
            body="",
        )
        for name in rules_names
    ]
    session.add_all(private_rules)

    await session.commit()

    yield public_rules, private_rules, user


@pytest.mark.anyio
async def test_admin_can_access_private_modules(
    session: AsyncSession, objects: DbTestObjects
) -> None:
    _, private_modules, _ = objects
    result = await pagination(SUDO, 0, 100, 12345, session)
    modules = set(module for module in result if module.private is True)

    assert modules.issuperset(private_modules)


@pytest.mark.anyio
async def test_random_order(session: AsyncSession, objects: DbTestObjects) -> None:
    modules, _, user = objects
    result = await pagination(user, 0, 5, 12345, session)

    assert isinstance(result, list)

    assert len(result) == 5

    # Assert that the order of the modules is different from the original order
    assert result != modules


@pytest.mark.anyio
async def test_negative_limit_or_page_number(
    session: AsyncSession, objects: DbTestObjects
) -> None:
    *_, user = objects
    result = await pagination(user, -1, 5, 12345, session)

    assert isinstance(result, list)
    assert len(result) == 0

    result = await pagination(user, 1, -1, 12345, session)

    assert isinstance(result, list)
    assert len(result) == 0


@pytest.mark.anyio
async def test_zero_limit(session: AsyncSession, objects: DbTestObjects) -> None:
    *_, user = objects
    result = await pagination(user, 1, 0, 12345, session)

    assert isinstance(result, list)
    assert len(result) == 0


@pytest.mark.anyio
async def test_page_greater_than_available_modules(
    session: AsyncSession, objects: DbTestObjects
) -> None:
    *_, user = objects
    result = await pagination(user, 100, 5, 12345, session)

    assert isinstance(result, list)
    assert len(result) == 0


@pytest.mark.anyio
async def test_full_text_search_with_empty_query(
    session: AsyncSession, objects: DbTestObjects
) -> None:
    *_, user = objects
    result = await full_text_search(user, "", session)
    assert isinstance(result, list)
    assert len(result) == 0


@pytest.mark.anyio
async def test_get_rule_set_by_name(
    session: AsyncSession, objects: DbTestObjects
) -> None:
    public_rules, _, user = objects
    result = await get_rule_set_by_name(
        user.displayname, public_rules[0].name, user, session
    )
    assert result == public_rules[0]
