"""
SPDX-License-Identifier: EUPL-1.2
"""

from io import BufferedReader
from typing import Generator as Gener

import pytest
from httpx import AsyncClient


@pytest.fixture
def sch_test_file(test_folder: str) -> Gener[BufferedReader, None, None]:
    """Get test file."""
    f = open(f"{test_folder}/kicad_projects/ferret/control.kicad_sch", "rb")
    yield f
    f.close()


@pytest.fixture
def svg_for_sch_test_file(test_folder: str) -> Gener[str, None, None]:
    """Get svg for test file."""
    f = open(f"{test_folder}/kicad_projects/ferret/control.svg", "r", encoding="utf-8")
    yield f.read()
    f.close()


@pytest.fixture
def themed_svg_for_sch_test_file(test_folder: str) -> Gener[str, None, None]:
    """Get svg for test file."""
    f = open(
        f"{test_folder}/kicad_projects/ferret/control_eagle.svg", "r", encoding="utf-8"
    )
    yield f.read()
    f.close()


@pytest.mark.anyio
async def test_no_payload(client: AsyncClient) -> None:
    response = await client.post("render/file", headers={"Webauth-User": "test"})
    assert response.status_code == 422


@pytest.mark.anyio
async def test_invalid_file_ext(client: AsyncClient) -> None:
    response = await client.post(
        "render/file",
        files={"file": ("test.txt", b"")},
        headers={"Webauth-User": "test"},
    )
    assert response.status_code == 422


@pytest.mark.anyio
async def test_invalid_file_encoding(client: AsyncClient) -> None:
    response = await client.post(
        "render/file",
        files={
            "file": (
                "test.kicad_sch",
                "".encode("utf-16"),
            )
        },
        headers={"Webauth-User": "test"},
    )
    assert response.status_code == 422
    assert (
        response.json()["detail"]
        == "Could not decode file as UTF-8: 'utf-8' codec can't decode byte 0xff in position 0: invalid start byte."
    )


@pytest.mark.anyio
async def test_rendering_sch(
    client: AsyncClient,
    sch_test_file: BufferedReader,
    svg_for_sch_test_file: str,
) -> None:
    """Test rendering a kicad_sch file."""
    response = await client.post(
        "render/file",
        files={
            "file": (
                "test.kicad_sch",
                sch_test_file,
            )
        },
        headers={"Webauth-User": "test"},
    )
    assert response.status_code == 200

    assert response.content.decode("utf-8") == svg_for_sch_test_file


@pytest.mark.anyio
async def test_rendering_sch_with_theme(
    client: AsyncClient,
    sch_test_file: BufferedReader,
    themed_svg_for_sch_test_file: str,
) -> None:
    """Test rendering a kicad_sch file."""
    response = await client.post(
        "render/file",
        files={
            "file": (
                "test.kicad_sch",
                sch_test_file,
            )
        },
        params={"theme": "eagle_dark"},
        headers={"Webauth-User": "test"},
    )
    assert response.status_code == 200

    assert response.content.decode("utf-8") == themed_svg_for_sch_test_file


@pytest.mark.anyio
async def test_empty_file(client: AsyncClient) -> None:
    """Test rendering an empty file."""
    response = await client.post(
        "render/file",
        files={
            "file": (
                "test.kicad_sch",
                b"",
            )
        },
        headers={"Webauth-User": "test"},
    )
    assert response.status_code == 422

    assert response.json()["detail"] == "unexpected EOF"


@pytest.mark.anyio
async def test_unsupported_version(
    sch_test_file: BufferedReader, client: AsyncClient
) -> None:
    """Test rendering unsupported version"""
    content = sch_test_file.read()
    content = content.replace(b"(version 20230121)", b"(version 20200931)")
    response = await client.post(
        "render/file",
        files={
            "file": (
                "test.kicad_sch",
                content,
            )
        },
        headers={"Webauth-User": "test"},
    )
    assert response.status_code == 422

    assert (
        response.json()["detail"]
        == "from_list [Only the stable KiCad 7 schematic file format i.e. "
        "'20230121' is supported. Got '20200931'. Please open and re-save the file with KiCad 7 if you can.]"
    )


@pytest.mark.anyio
async def test_unsupported_file_type(
    sch_test_file: BufferedReader, client: AsyncClient
) -> None:
    """Test rendering unsupported file type"""
    response = await client.post(
        "render/file",
        files={
            "file": (
                "test.txt",
                sch_test_file,
            )
        },
        headers={"Webauth-User": "test"},
    )
    assert response.status_code == 422

    assert response.json()["detail"] == [
        {
            "loc": ["body", "file"],
            "msg": "Expected a `kicad_sch` or `kicad_pcb` file, got unexpected file: `test.txt`.",
            "type": "value_error",
        }
    ]


@pytest.fixture
def pcb_test_file(test_folder: str) -> Gener[BufferedReader, None, None]:
    """Get test file."""
    f = open(f"{test_folder}/kicad_projects/ferret/ferret.kicad_pcb", "rb")
    yield f
    f.close()


@pytest.fixture
def svg_for_pcb_test_file(test_folder: str) -> Gener[str, None, None]:
    """Get svg for test file."""
    f = open(f"{test_folder}/kicad_projects/ferret/ferret.svg", "r", encoding="utf-8")
    yield f.read()
    f.close()


@pytest.mark.anyio
async def test_rendering_pcb(
    pcb_test_file: BufferedReader, svg_for_pcb_test_file: str, client: AsyncClient
) -> None:
    """Test rendering a kicad_pcb file."""
    response = await client.post(
        "render/file",
        files={
            "file": (
                "test.kicad_pcb",
                pcb_test_file,
            )
        },
        headers={"Webauth-User": "test"},
    )
    assert response.status_code == 200

    # with open("app/tests/kicad_projects/ferret/ferret.svg", "w") as f:
    #     f.write(response.content.decode("utf-8"))
    expected = svg_for_pcb_test_file
    actual = response.content.decode("utf-8")

    assert actual == expected
