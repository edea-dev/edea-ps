import random
from typing import Any
from typing import Generator as Gener
from urllib.parse import urlparse

import pytest
import pytest_mock
import yaml
from httpx import AsyncClient, get

from app.db.models.module import ModuleNotFound


@pytest.fixture(scope="module")
def modules_from_repo_edea_yaml() -> Gener[dict[str, Any], None, None]:
    """Get the edea.yaml file from the repo."""
    content = get(
        "https://gitlab.com/edea-dev/test-modules/-/raw/main/edea.yml?ref_type=heads"
    ).content
    yield yaml.safe_load(content)["modules"]


@pytest.mark.anyio
async def test_repo_without_edea_yml_file(client: AsyncClient) -> None:
    response = await client.post(
        "/submit",
        json={
            "repo_url": "https://gitlab.com/edea-dev/edea-ps",
        },
        headers={"Webauth-User": "new-module-user"},
    )

    assert response.status_code == 422


@pytest.mark.anyio
async def test_new_module_from_unsupported_git_host(client: AsyncClient) -> None:
    response = await client.post(
        "/submit",
        json={
            "repo_url": "https://example.com/edea-dev/edea-ps",
        },
        headers={"Webauth-User": "new-module-user"},
    )

    assert response.status_code == 422


@pytest.mark.anyio
async def test_new_module_from_non_git_repo_url(client: AsyncClient) -> None:
    response = await client.post(
        "/submit",
        json={
            "repo_url": "https://gitlab.com/help/",
        },
        headers={"Webauth-User": "new-module-user"},
    )
    assert response.status_code == 422
    assert (
        response.json()["detail"]
        == "failed to clone repository at https://gitlab.com/help/"
    )


@pytest.mark.anyio
async def test_new_module_from_valid_host_but_invalid_path(client: AsyncClient) -> None:
    response = await client.post(
        "/submit",
        json={
            "repo_url": "https://gitlab.com",
        },
        headers={"Webauth-User": "new-module-user"},
    )
    assert response.status_code == 422


@pytest.mark.anyio
async def test_repo_with_with_missing_module_key_in_edea_yml(
    client: AsyncClient, mocker: pytest_mock.MockFixture
) -> None:
    with mocker.patch(
        "app.services.repository.EDeAConfig.from_yaml",
        side_effect=yaml.YAMLError("invalid yaml"),
    ):
        response = await client.post(
            "/submit",
            json={
                "repo_url": "https://gitlab.com/edea-dev/malformed-test-modules",
            },
            headers={"Webauth-User": "new-module-user"},
        )
    assert response.status_code == 422


@pytest.mark.anyio
async def test_repo_with_with_invalid_module_configuration(
    client: AsyncClient, mocker: pytest_mock.MockFixture
) -> None:
    response = await client.post(
        "/submit",
        json={
            "repo_url": "https://gitlab.com/edea-dev/malformed-test-modules",
        },
        headers={"Webauth-User": "new-module-user-2"},
    )

    assert response.status_code == 422


@pytest.mark.anyio
async def test_create_correct_number_of_modules_from_repo(
    new_modules: list[dict[str, Any]],
    modules_from_repo_edea_yaml: dict[str, dict[str, Any]],
) -> None:
    """Test that the correct number of modules are created from the repo."""
    assert len(new_modules) == len(modules_from_repo_edea_yaml)


@pytest.mark.anyio
async def test_create_modules_with_right_description(
    new_modules: list[dict[str, Any]],
    modules_from_repo_edea_yaml: dict[str, dict[str, Any]],
) -> None:
    """Test that the correct number of modules are created from the repo."""
    for m1, m2 in zip(new_modules, modules_from_repo_edea_yaml.values()):
        assert m1.get("description", None) == m2.get("description", None)


@pytest.mark.anyio
async def test_create_modules_with_right_parameters(
    new_modules: list[dict[str, Any]],
    modules_from_repo_edea_yaml: dict[str, dict[str, Any]],
) -> None:
    """Test that the modules has the same parameters defined in edea.yml."""
    for m1, m2 in zip(new_modules, modules_from_repo_edea_yaml.values()):
        m1_params = m1.get("parameters", dict())
        m2_params = m2.get("params", dict())

        assert set((k, str(v)) for k, v in m2_params.items()) == set(
            (param["type"], param["value"]) for param in m1_params
        )


@pytest.mark.anyio
async def test_search_page_size(
    client: AsyncClient, new_modules: list[dict[str, Any]]
) -> None:
    modules = (
        await client.post(
            "/module/search",
            json={
                "page": 0,
                "limit": 10,
                "seed": 0,
            },
        )
    ).json()["hits"]
    ids_10 = [module["id"] for module in modules]
    assert len(ids_10) > 0

    modules = (
        await client.post(
            "/module/search",
            json={
                "page": 0,
                "limit": 5,
                "seed": 0,
            },
        )
    ).json()["hits"]

    ids_first_5 = [module["id"] for module in modules]

    assert len(ids_first_5) > 0

    modules = (
        await client.post(
            "/module/search",
            json={
                "page": 1,
                "limit": 5,
                "seed": 0,
            },
        )
    ).json()["hits"]

    ids_last_5 = [module["id"] for module in modules]
    assert len(ids_last_5) > 0

    assert ids_10 == ids_first_5 + ids_last_5


@pytest.mark.anyio
async def test_search_pagination_page_consistency(
    client: AsyncClient, new_modules: list[dict[str, Any]]
) -> None:
    modules_1 = (
        await client.post(
            "/module/search",
            json={
                "page": 0,
                "limit": 10,
                "seed": 0,
            },
        )
    ).json()["hits"]

    modules_2 = (
        await client.post(
            "/module/search",
            json={
                "page": 0,
                "limit": 10,
                "seed": 0,
            },
        )
    ).json()["hits"]

    assert [module["id"] for module in modules_1] == [
        module["id"] for module in modules_2
    ]

    modules_3 = (
        await client.post(
            "/module/search",
            json={
                "page": 0,
                "limit": 10,
                "seed": 1,
            },
        )
    ).json()["hits"]

    modules_4 = (
        await client.post(
            "/module/search",
            json={
                "page": 0,
                "limit": 10,
                "seed": 1,
            },
        )
    ).json()["hits"]

    assert [module["id"] for module in modules_3] == [
        module["id"] for module in modules_4
    ]


@pytest.mark.anyio
async def test_modules_full_text_search(
    client: AsyncClient, new_modules: list[dict[str, Any]]
) -> None:
    q = random.choice(new_modules)["description"]
    modules = (await client.post("/module/search", json={"query": q})).json()["hits"]

    assert modules[0]["description"] == q


@pytest.mark.anyio
async def test_module_search_username(
    client: AsyncClient, new_modules: list[dict[str, Any]]
) -> None:
    user = new_modules[0]["user"]["displayname"]
    modules = (await client.post("/module/search", json={"username": user})).json()[
        "hits"
    ]

    assert len(modules) == 7
    assert all(module["user"]["displayname"] == user for module in modules)


@pytest.mark.anyio
async def test_module_search_repo_url(
    client: AsyncClient, new_modules: list[dict[str, Any]]
) -> None:
    repo_url = new_modules[0]["repository"]["url"]
    path = urlparse(repo_url).path
    modules = (await client.post("/module/search", json={"repo_url": path})).json()[
        "hits"
    ]

    assert len(modules) == 7
    assert all(module["repository"]["url"] == repo_url for module in modules)


@pytest.mark.anyio
async def test_get_non_existent_module(
    client: AsyncClient, new_modules: list[dict[str, Any]]
) -> None:
    response = await client.get("/module/someones/repo/module")
    assert response.status_code == 404


@pytest.mark.anyio
async def test_repository_unique_per_user(
    client: AsyncClient,
) -> None:
    response = await client.post(
        "/submit",
        json={
            "repo_url": "https://gitlab.com/edea-dev/test-modules.git",
            "private": True,
        },
        headers={"Webauth-User": "unique-module-user"},
    )

    assert response.status_code == 201

    response = await client.post(
        "/submit",
        json={
            "repo_url": "https://gitlab.com/edea-dev/test-modules.git",
            "private": True,
        },
        headers={"Webauth-User": "unique-module-user"},
    )

    assert response.status_code == 409


@pytest.mark.anyio
async def test_search_modules_parametric(
    client: AsyncClient, new_modules: list[dict[str, Any]]
) -> None:
    params = {
        "parameters": [
            {
                "type": "u_arch",
                "values": ["ARM Cortex-M3"],
            }
        ]
    }

    modules = (await client.post("/module/search", json=params)).json()["hits"]
    assert any(
        param["type"] == "u_arch" and param["value"] == "ARM Cortex-M3"
        for param in modules[0]["parameters"]
    )


@pytest.mark.anyio
async def test_search_get_all_parameters(
    client: AsyncClient, new_modules: list[dict[str, Any]]
) -> None:
    response = await client.get("/module/search/all-parameters")

    all_parameters = [
        {"type": "i_out_max", "values": ["1", "4", "0.5"]},
        {"type": "u_arch", "values": ["ARM Cortex-M3"]},
        {"type": "v_in_abs_max", "values": ["60"]},
        {"type": "v_in_abs_min", "values": ["-40"]},
        {"type": "v_in_max", "values": ["20", "17", "15", "32"]},
        {"type": "v_in_min", "values": ["5.5", "4"]},
        {"type": "v_in_nom", "values": ["3.3"]},
        {"type": "v_out_nom", "values": ["3.3", "5"]},
    ]
    assert response.json() == all_parameters


@pytest.mark.anyio
async def test_delete_module(
    client: AsyncClient,
    new_modules: list[dict[str, Any]],
    to_be_delete_module_name: str,
) -> None:
    response = await client.delete(
        f"/module/{to_be_delete_module_name}",
        headers={"Webauth-User": "new-module-user"},
    )
    assert response.status_code == 204

    response = await client.get(f"/module/{to_be_delete_module_name}")
    assert response.status_code == 404


@pytest.mark.anyio
async def test_delete_non_existent_module(
    client: AsyncClient,
) -> None:
    response = await client.delete(
        "/module/someones/repo/module", headers={"Webauth-User": "new-module-user"}
    )
    assert response.status_code == 404


@pytest.mark.anyio
async def test_get_readme(
    client: AsyncClient, new_modules: list[dict[str, Any]]
) -> None:
    m = random.choice(new_modules)
    module_full_name = (
        f"{m['user']['displayname']}/{m['repository']["name"]}/{m['name']}"
    )
    response = await client.get(f"/module/{module_full_name}/readme")
    assert response.status_code == 200

    assert response.headers["content-type"] == "text/html; charset=utf-8"


@pytest.mark.anyio
async def test_get_non_existent_module_readme(
    client: AsyncClient,
) -> None:
    response = await client.get("/module/someone/repo/module/readme")
    assert response.status_code == 404


@pytest.mark.anyio
async def test_search_for_readme_path(
    client: AsyncClient, new_modules: list[dict[str, Any]]
) -> None:
    """If the readme path specified in edea.yml is not found, search for a readme file in the module path."""
    m = list(filter(lambda m: m["name"] == "5vpol", new_modules))[0]
    module_full_name = (
        f"{m['user']['displayname']}/{m['repository']['name']}/{m['name']}"
    )
    response = await client.get(f"/module/{module_full_name}/readme")

    assert (
        "I spent half a year validating this IC design before it was released to the market, and it is very robust."
        in response.text
    )


@pytest.mark.anyio
async def test_readme_rendering_for_gitlab(
    client: AsyncClient, new_modules: list[dict[str, Any]]
) -> None:
    m = list(filter(lambda m: m["name"] == "5vpol", new_modules))[0]
    module_full_name = (
        f"{m['user']['displayname']}/{m['repository']['name']}/{m['name']}"
    )
    response = await client.get(f"/module/{module_full_name}/readme")

    # readme images pointing the raw urls
    assert (
        '<img alt="Photo of the Fully Automated 17-V PoL" decoding="async" loading="lazy" '
        'src="https://gitlab.com/edea-dev/test-modules/-/raw/HEAD/5vpol/doc/pcb.png" />'
        in response.text
    )


@pytest.mark.anyio
async def test_readme_rendering_for_github(client: AsyncClient) -> None:
    modules = (
        await client.post(
            "/submit",
            json={
                "repo_url": "https://github.com/kitspace-forks/test-modules",
            },
            headers={"Webauth-User": "github-user"},
        )
    ).json()

    m = list(filter(lambda m: m["name"] == "5vpol", modules))[0]
    module_full_name = (
        f"{m['user']['displayname']}/{m['repository']['name']}/{m['name']}"
    )

    response = await client.get(f"/module/{module_full_name}/readme")

    # readme images pointing the raw urls
    assert (
        '<img alt="Photo of the Fully Automated 17-V PoL" decoding="async" loading="lazy" '
        'src="https://raw.githubusercontent.com/kitspace-forks/test-modules/HEAD/5vpol/doc/pcb.png" />'
        in response.text
    )


@pytest.mark.anyio
async def test_module_without_readme_file(
    mocker: pytest_mock.MockFixture,
    client: AsyncClient,
    new_modules: list[dict[str, Any]],
) -> None:
    m = new_modules[0]
    module_full_name = (
        f"{m['user']['displayname']}/{m['repository']['name']}/{m['name']}"
    )
    with mocker.patch(
        # no readme found even when searching the repo
        "app.db.queries.module.get_module_by_name",
        side_effect=ModuleNotFound(
            f"Could not find module with name `{m['name']}` for user `{m['user']['displayname']}`."
        ),
    ):
        response = await client.get(f"/module/{module_full_name}/readme")

    assert response.status_code == 404
    assert (
        response.json()["detail"]
        == f"Could not find module with name `{m['name']}` for user `{m['user']['displayname']}`."
    )


@pytest.fixture(scope="module")
def module_commits() -> Gener[list[dict[str, str]], None, None]:
    response = get(
        "https://gitlab.com/api/v4/projects/25939181/repository/commits?per_page=100"
    ).json()

    yield [{"ref": commit["id"], "message": commit["message"]} for commit in response]


@pytest.mark.anyio
async def test_get_module_commits(
    client: AsyncClient, module_commits: list[dict[str, str]], new_module_name: str
) -> None:
    """Module commits should be a subset of the repo commits."""
    repo_commits = (await client.get(f"/module/{new_module_name}/commits")).json()

    hashable_module_commits = [
        (commit["ref"], commit["message"]) for commit in module_commits
    ]
    hashable_repo_commits = [
        (commit["ref"], commit["message"]) for commit in repo_commits
    ]

    assert set(hashable_repo_commits).issubset(hashable_module_commits)


@pytest.mark.anyio
async def test_get_non_existent_module_commits(
    client: AsyncClient,
) -> None:
    response = await client.get("/module/someones/repo/module/commits")
    assert response.status_code == 404


@pytest.mark.anyio
async def test_get_module_violations(client: AsyncClient, new_module_name: str) -> None:
    response = await client.get(f"/module/{new_module_name}/violations")
    assert response.status_code == 200
    body = response.json()
    assert isinstance(body["dr"]["violations"], list)
