"""
SPDX-License-Identifier: EUPL-1.2
"""

from io import BytesIO
from typing import Generator as Gener
from zipfile import ZipFile

import pytest
import pytest_mock
from httpx import AsyncClient

from app.db.models.module import ModuleNotFound


@pytest.fixture
def svg_for_sch_test_file(test_folder: str) -> Gener[str, None, None]:
    """Get svg for test file."""
    f = open(
        f"{test_folder}/kicad_projects/LDO/HT7533-1/HT7533-1.kicad_sch.svg",
        "r",
        encoding="utf-8",
    )
    yield f.read()
    f.close()


@pytest.mark.anyio
async def test_rendering_module(client: AsyncClient, new_module_name: str) -> None:
    """Test rendering a file in EDeA module."""
    response = await client.get(
        f"/render/{new_module_name}",
        headers={"Webauth-User": "new-module-user"},
    )

    response.raise_for_status()
    with ZipFile(BytesIO(response.content), "r") as zip_file:
        # check if the archive contains the correct files
        assert "pcb.svg" in zip_file.namelist()
        assert "schematic.svg" in zip_file.namelist()


@pytest.mark.anyio
async def test_not_found_module(
    mocker: pytest_mock.MockFixture, client: AsyncClient, new_module_name: str
) -> None:
    with mocker.patch(
        "app.db.queries.get_module_by_name",
        side_effect=ModuleNotFound(
            f"Could not find module with name `{new_module_name}`."
        ),
    ):
        response = await client.get(
            f"/render/{new_module_name}",
            headers={"Webauth-User": "new-module-user"},
        )

    assert response.status_code == 404
    assert (
        response.json()["detail"]
        == f"Could not find module with name `{new_module_name}`."
    )
