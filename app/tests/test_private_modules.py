import pytest
from httpx import AsyncClient

from app.config import CONFIG


@pytest.mark.anyio
async def test_get_private_module(
    client: AsyncClient,
) -> None:
    response = await client.post(
        "/submit",
        json={
            "repo_url": "https://gitlab.com/edea-dev/test-modules.git",
            "private": True,
        },
        headers={"Webauth-User": "private-module-user"},
    )

    assert response.status_code == 201
    modules = response.json()
    m = modules[0]
    moduel_full_name = (
        f'{m["user"]["displayname"]}/{m["repository"]["name"]}/{m["name"]}'
    )
    response = await client.get(f"/module/{moduel_full_name}")
    assert response.status_code == 404

    response = await client.get(
        f"/module/{moduel_full_name}",
        headers={"Webauth-User": "private-module-user"},
    )

    assert response.status_code == 200
    assert response.json() == m


@pytest.mark.anyio
async def test_searches_private_modules(client: AsyncClient) -> None:
    private_modules_1 = (
        await client.post(
            "/submit",
            json={
                "repo_url": "https://gitlab.com/edea-dev/test-modules.git",
                "private": True,
            },
            headers={"Webauth-User": "super-secret-user-1"},
        )
    ).json()

    private_modules_2 = (
        await client.post(
            "/submit",
            json={
                "repo_url": "https://gitlab.com/edea-dev/test-modules.git",
                "private": True,
            },
            headers={"Webauth-User": "super-secret-user-2"},
        )
    ).json()

    response_1 = (
        await client.post(
            "/module/search",
            json={
                "query": private_modules_1[0]["name"],
                "page": 0,
                "limit": 10,
            },
            headers={"Webauth-User": "super-secret-user-1"},
        )
    ).json()["hits"]

    assert private_modules_1[0]["id"] in [
        module["id"] for module in response_1
    ], "should return private modules for super-secret-user-1"
    response_2 = (
        await client.post(
            "/module/search",
            json={
                "query": private_modules_2[0]["name"],
                "page": 0,
                "limit": 10,
            },
            headers={"Webauth-User": "super-secret-user-2"},
        )
    ).json()["hits"]

    assert private_modules_2[0]["id"] in [
        module["id"] for module in response_2
    ], "should return private modules for super-secret-user-2"

    response_3 = (
        await client.post(
            "/module/search",
            json={
                "query": private_modules_2[0]["name"],
                "page": 0,
                "limit": 10,
            },
        )
    ).json()["hits"]

    assert [
        module for module in response_3 if module["private"]
    ] == [], "should not return private modules for anonymous user"


@pytest.mark.anyio
async def test_create_modules_from_private_repo(client: AsyncClient) -> None:
    modules = (
        await client.post(
            "/submit",
            json={
                "repo_url": "https://gitlab.com/edea-dev/private-test-modules",
                "access_token": CONFIG.gitlab_private_token,
            },
            headers={"Webauth-User": "super-secret-user-3"},
        )
    ).json()

    assert len(modules) == 7
