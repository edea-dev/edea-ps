import pytest
from httpx import AsyncClient

from app import app


@pytest.mark.anyio
async def test_root_redirects_to_docs() -> None:
    async with AsyncClient(
        app=app, base_url="http://test", follow_redirects=True
    ) as client:
        response = await client.get("/")

    assert response.url.path == "/docs"
