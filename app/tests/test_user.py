from typing import Any

import pytest
from httpx import AsyncClient


@pytest.mark.anyio
async def test_user_modules(
    client: AsyncClient, new_modules: list[dict[str, Any]]
) -> None:
    username = new_modules[0]["user"]["displayname"]
    response = await client.get(f"/user/{username}/modules")
    assert response.json() == new_modules


@pytest.mark.anyio
async def test_non_existent_user_modules(
    client: AsyncClient, new_modules: list[dict[str, Any]]
) -> None:
    response = await client.get("/user/someone/modules")
    assert response.status_code == 404


@pytest.mark.anyio
async def test_current_user_modules(
    client: AsyncClient, new_modules: list[dict[str, Any]]
) -> None:
    response = await client.get(
        "/user/modules", headers={"Webauth-User": "new-module-user"}
    )
    assert response.json() == new_modules
