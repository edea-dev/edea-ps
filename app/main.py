"""
An API for EDeA portal server.
SPDX-License-Identifier: EUPL-1.2
"""

from contextlib import asynccontextmanager
from typing import AsyncGenerator
from urllib.parse import urlparse

import sqlalchemy.exc
from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse, RedirectResponse
from starlette.middleware.sessions import SessionMiddleware

from app.config import CONFIG
from app.db import connection, engine, fts, queries
from app.routers import static, v1
from app.services import cache, tasks
from app.services.auth import AuthenticationMiddleware

tags_metadata = [
    {
        "name": "auth",
        "description": "User authentication and authorization.",
    },
    {
        "name": "meta",
        "description": "Health check, and docs.",
    },
    {
        "name": "module",
        "description": "Submit, search, read parameters, commits, violations of modules.",
    },
    {
        "name": "render",
        "description": "Render a KiCad schematic or pcb file or "
        "an EDeA module as an svg file.",
    },
    {
        "name": "rules",
        "description": "Submit, search and read rulesets.",
    },
    {
        "name": "static",
        "description": "Serve pre-rendered static files (e.g., pcb preview).",
    },
    {
        "name": "user",
        "description": "Auth and user owned resources.",
    },
]


@asynccontextmanager
async def lifespan(app: FastAPI) -> AsyncGenerator[None, None]:  # pragma: no cover
    """Start and stop the database connection pool."""
    # disable pylint warnings for unused arguments
    # and redefined outer name for this function
    # pylint: disable=W0621, W0613
    if not tasks.broker.is_worker_process:
        async with engine.begin() as conn:
            await conn.run_sync(connection.Base.metadata.create_all)
            await queries.category.create_categories()
            await connection.apply_migrations(conn)
        await cache.setup()
        fts.create_fts()
        await tasks.setup()
    yield
    await tasks.teardown()
    await connection.engine.dispose()


app = FastAPI(
    title="EDeA Portal Server",
    contact={
        "name": "EDeA portal server issues",
        "url": "https://gitlab.com/edea-dev/edea-ps/-/issues/new",
    },
    description="API for EDeA portal server.",
    license_info={
        "name": "EUPL 1.2",
        "url": "https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12",
    },
    openapi_tags=tags_metadata,
    lifespan=lifespan,
    servers=[
        {
            "x-internal": True,
            "url": "http://localhost:8000",
            "description": "Local development server.",
        },
        {
            "x-internal": False,
            "url": "https://api.edea.test",
            "description": "Local development server.",
        },
    ],
)

app.add_middleware(AuthenticationMiddleware)
app.add_middleware(
    SessionMiddleware,
    secret_key=CONFIG.session_secret,
    https_only=True,
    # This is the easiest way to set the cookie domain the toplevel domain
    path=f"/; Domain={urlparse(CONFIG.frontend_url).hostname};",
)
app.include_router(v1.router)
app.include_router(static.router)


@app.exception_handler(sqlalchemy.exc.IntegrityError)
async def sqlalchemy_integrity_error(
    _: Request, exc: sqlalchemy.exc.IntegrityError
) -> JSONResponse:
    msg = str(exc.orig)
    err = {"error": {"message": msg}}

    if msg.startswith("UNIQUE constraint failed"):
        constraint = msg[25:]
        if "." in constraint:
            constraint = constraint.split(".")[1]
        err["error"]["field"] = constraint
        err["error"]["type"] = "unique_violation"

    return JSONResponse(
        status_code=409,
        content=err,
    )


@app.get("/", tags=["meta"])
async def root() -> RedirectResponse:
    "Redirect to docs when accessing homepage."
    return RedirectResponse("/docs")


@app.get("/health", tags=["meta"])
async def health() -> JSONResponse:
    "Check the health of the server."
    return JSONResponse(
        content={"status": "pass"}, media_type="application/health+json"
    )
