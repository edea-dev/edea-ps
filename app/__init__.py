"""
The EDeA Portal API.
SPDX-License-Identifier: EUPL-1.2
"""

from .main import app  # noqa: F401
