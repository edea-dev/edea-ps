from fastapi import APIRouter
from fastapi.responses import FileResponse

from app.config import CONFIG
from app.services.auth import OptionalCurrentUser

router = APIRouter()


@router.get("/static/{file_path:path}", tags=["static"])
async def function(actor: OptionalCurrentUser, file_path: str) -> FileResponse:
    """Serve static files."""
    # TODO: private files should be served only to the owner
    response = FileResponse(f"{CONFIG.assets_path}/{file_path}")
    response.headers["Cache-Control"] = "max-age=31536000"
    return response
