v1_modules = [
    {
        "id": "a8924f14-b826-4c32-890e-0c15e1c38b7f",
        "user": {
            "id": "3c825d0f-4067-4526-a0ae-b88c9bfef5e4",
            "subject": "a",
            "displayname": "",
            "groups": [],
            "roles": [],
        },
        "short_code": None,
        "private": False,
        "repository": {
            "url": "https://gitlab.com/edea-dev/test-modules",
            "name": "test-modules",
        },
        "name": "5vpol",
        "description": "16-V Input 3-A Output Point-of-Load with TPS62135",
        "category_id": "1",
        "metadata_": {
            "area_mm": 1050,
            "width_mm": 42,
            "height_mm": 25,
            "count_copper_layer": 1,
            "sheets": 0,
            "count_part": 31,
            "count_unique_part": 27,
            "parts": {
                "00000000-0000-0000-0000-00005d938883": {
                    "Reference": "J2",
                    "Value": "OUT",
                    "Footprint": "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Horizontal",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005d948501": {
                    "Reference": "J1",
                    "Value": "IN",
                    "Footprint": "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Horizontal",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005d94cdd6": {
                    "Reference": "D1",
                    "Value": "SMAJ15A-13-F",
                    "Footprint": "Diode_SMD:D_SMA",
                    "Datasheet": "",
                },
                "00000000-0000-0000-0000-00005d9beba1": {
                    "Reference": "H1",
                    "Value": "MountingHole_Pad",
                    "Footprint": "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005d9d6353": {
                    "Reference": "G1",
                    "Value": "Fully_Automated_Logo",
                    "Footprint": "Automated:fully_automated_logo_soldermask",
                    "Datasheet": "",
                },
                "00000000-0000-0000-0000-00005da23798": {
                    "Reference": "R3",
                    "Value": "2k2",
                    "Footprint": "Resistor_SMD:R_0603_1608Metric",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005da2463f": {
                    "Reference": "D2",
                    "Value": "LED_Small_ALT",
                    "Footprint": "LED_SMD:LED_0603_1608Metric",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252ab0": {
                    "Reference": "U1",
                    "Value": "TPS62135",
                    "Footprint": "Automated:RGX_TI_VQFN-HR11",
                    "Datasheet": "http://www.ti.com/lit/ds/symlink/tps62135.pdf",
                },
                "00000000-0000-0000-0000-00005e252ab6": {
                    "Reference": "L1",
                    "Value": "XGL4020-102ME_",
                    "Footprint": "Inductor_SMD:L_Coilcraft_XxL4020",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252abc": {
                    "Reference": "R5",
                    "Value": "240k",
                    "Footprint": "Resistor_SMD:R_0603_1608Metric",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252ac2": {
                    "Reference": "C4",
                    "Value": "22pF/50V",
                    "Footprint": "Capacitor_SMD:C_0603_1608Metric",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252aed": {
                    "Reference": "C5",
                    "Value": "10uF/6.3V",
                    "Footprint": "Capacitor_SMD:C_0805_2012Metric",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252afb": {
                    "Reference": "C2",
                    "Value": "2.2uF/25V",
                    "Footprint": "Capacitor_SMD:C_0805_2012Metric",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252b0e": {
                    "Reference": "C1",
                    "Value": "2.2uF/25V",
                    "Footprint": "Capacitor_SMD:C_0805_2012Metric",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252b1a": {
                    "Reference": "C3",
                    "Value": "100nF/25V",
                    "Footprint": "Capacitor_SMD:C_0603_1608Metric",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252b23": {
                    "Reference": "JP1",
                    "Value": "SolderJumper_3_Bridged12",
                    "Footprint": "Jumper:SolderJumper-3_P1.3mm_Bridged12_RoundedPad1.0x1.5mm_NumberLabels",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252b3c": {
                    "Reference": "NT1",
                    "Value": "Net-Tie_2",
                    "Footprint": "NetTie:NetTie-2_SMD_Pad2.0mm",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252b5c": {
                    "Reference": "C6",
                    "Value": "10uF/6.3V",
                    "Footprint": "Capacitor_SMD:C_0805_2012Metric",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252b6b": {
                    "Reference": "C7",
                    "Value": "10uF/6.3V",
                    "Footprint": "Capacitor_SMD:C_0805_2012Metric",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252b7a": {
                    "Reference": "PG1",
                    "Value": "PG",
                    "Footprint": "TestPoint:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252b81": {
                    "Reference": "EN1",
                    "Value": "EN",
                    "Footprint": "TestPoint:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252b8e": {
                    "Reference": "TP1",
                    "Value": "GND",
                    "Footprint": "TestPoint:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252b94": {
                    "Reference": "VIN1",
                    "Value": "VIN",
                    "Footprint": "TestPoint:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252b9c": {
                    "Reference": "MODE1",
                    "Value": "MODE",
                    "Footprint": "TestPoint:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252ba4": {
                    "Reference": "JP2",
                    "Value": "SolderJumper_2_Bridged",
                    "Footprint": "Jumper:SolderJumper-2_P1.3mm_Bridged_RoundedPad1.0x1.5mm",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252bab": {
                    "Reference": "R6",
                    "Value": "39k",
                    "Footprint": "Resistor_SMD:R_0603_1608Metric",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252bb1": {
                    "Reference": "R4",
                    "Value": "dnp",
                    "Footprint": "Resistor_SMD:R_0603_1608Metric",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252bbc": {
                    "Reference": "VSEL1",
                    "Value": "VSEL",
                    "Footprint": "TestPoint:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252bc4": {
                    "Reference": "JP3",
                    "Value": "SolderJumper_2_Bridged",
                    "Footprint": "Jumper:SolderJumper-2_P1.3mm_Bridged_RoundedPad1.0x1.5mm",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252bd1": {
                    "Reference": "R2",
                    "Value": "10k",
                    "Footprint": "Resistor_SMD:R_0603_1608Metric",
                    "Datasheet": "~",
                },
                "00000000-0000-0000-0000-00005e252bd7": {
                    "Reference": "R1",
                    "Value": "100",
                    "Footprint": "Resistor_SMD:R_0603_1608Metric",
                    "Datasheet": "~",
                },
            },
        },
        "readme_path": "readme.md",
        "parameters": [],
    },
]


v1_search_parameter = {
    "type": "v_in_max",
    "values": ["32", "60", "-40"],
}

v1_search_parameters = [v1_search_parameter]

v1_rules = [
    {
        "id": "c63a15a8-51d2-4440-af51-0d9595c0bd14",
        "user_id": "9f313f90-470f-4ff4-bce1-21ab5c9898cd",
        "short_code": None,
        "private": False,
        "repository": {
            "id": "426469f1-6642-4197-9336-719ce1145342",
            "url": "https://gitlab.com/edea-dev/test-rules",
            "name": "test-rules",
        },
        "name": "JLCPCB-KiCad-DRC",
        "description": "LCPCB Design Rules for KiCad 7.0, implemented as Custom Rules in PCB Editor",
        "readme_path": "readme.md",
        "body": "(version 1)\n"
        "# Custom Design Rules (DRC) for KiCAD 7.0 (Stored in '<project>.kicad_dru' file).\n"
        "#\n"
        "# Matching JLCPCB capabilities: https://jlcpcb.com/capabilities/pcb-capabilities\n"
        "#\n"
        "# KiCad documentation: https://docs.kicad.org/master/id/pcbnew/pcbnew_advanced.html#custom_design_rules\n"
        "#\n"
        "# Inspiration\n"
        "# - https://gist.github.com/darkxst/f713268e5469645425eed40115fb8b49 (with comments)\n"
        "# - https://gist.github.com/denniskupec/e163d13b0a64c2044bd259f64659485e (with comments)\n"
        "\n"
        "# TODO new rule: NPTH pads.\n"
        "# Inner diameter of pad should be 0.4-0.5 mm larger than NPTH drill diameter.\n"
        '# JLCPCB: "We make NPTH via dry sealing film process, if customer would like a NPTH '
        "but around with pad/copper,"
        "our engineer will dig out around pad/copper about 0.2mm-0.25mm,"
        "otherwise the metal potion will be flowed into the hole and it becomes a PTH. "
        "(there will be no copper dig out optimization for single board)."
        "\n"
        "# TODO: new rule for plated slots: min diameter/width 0.5mm\n"
        '# JLCPCB: "The minimum plated slot width is 0.5mm, which is drawn with a pad."\n'
        "\n"
        "# TODO new rule: non-plated slots: min diameter/width 1.0mm\n"
        '# JLCPCB: "The minimum Non-Plated Slot Width is 1.0mm,'
        'please draw the slot outline in the mechanical layer(GML or GKO)"\n'
        "\n"
        '(rule "Track width, outer layer (1oz copper)"\n'
        "\t(layer outer)\n"
        "\t(condition \"A.Type == 'track'\")\n"
        "\t(constraint track_width (min 0.127mm))\n"
        ")\n"
        "\n"
        '(rule "Track spacing, outer layer (1oz copper)"\n'
        "\t(layer outer)\n"
        "\t(condition \"A.Type == 'track' && B.Type == A.Type\")\n"
        "\t(constraint clearance (min 0.127mm))\n"
        ")\n"
        "\n"
        '(rule "Track width, inner layer"\n'
        "\t(layer inner)\n"
        "\t(condition \"A.Type == 'track'\")\n"
        "\t(constraint track_width (min 0.09mm))\n"
        ")\n"
        "\n"
        '(rule "Track spacing, inner layer"\n'
        "\t(layer inner)\n"
        "\t(condition \"A.Type == 'track' && B.Type == A.Type\")\n"
        "\t(constraint clearance (min 0.09mm))\n"
        ")\n"
        "\n"
        '(rule "Silkscreen text"\n'
        '\t(layer "?.Silkscreen")\n'
        "\t(condition \"A.Type == 'Text' || A.Type == 'Text Box'\")\n"
        "\t(constraint text_thickness (min 0.15mm))\n"
        "\t(constraint text_height (min 1mm))\n"
        ")\n"
        "\n"
        '(rule "Pad to Silkscreen"\n'
        "\t(layer outer)\n"
        "\t(condition \"A.Type == 'pad' && B.Layer == '?.Silkscreen'\")\n"
        "\t(constraint silk_clearance (min 0.15mm))\n"
        ")\n"
        "\n"
        '(rule "Edge (routed) to track clearance"\n'
        "\t(condition \"A.Type == 'track'\")\n"
        "\t(constraint edge_clearance (min 0.3mm))\n"
        ")\n"
        "\n"
        '#(rule "Edge (v-cut) to track clearance"\n'
        "#\t(condition \"A.Type == 'track'\")\n"
        "#\t(constraint edge_clearance (min 0.4mm))\n"
        "#)\n"
        "\n"
        "# JLCPCB restrictions ambiguous:\n"
        '# Illustration: 0.2 mm, 1&2 layer: 0.3 mm, multilayer: "(0.15mm more costly)"\n'
        "# This rule handles diameter minimum and maximum for ALL holes.\n"
        "# Other specialized rules handle restrictions (e.g. Via, PTH, NPTH) \n"
        '(rule "Hole diameter"\n'
        "\t(constraint hole_size (min 0.2mm) (max 6.3mm))\n"
        ")\n"
        "\n"
        '(rule "Hole (NPTH) diameter"\n'
        "\t(layer outer)\n"
        '\t(condition "!A.isPlated()")\n'
        "\t(constraint hole_size (min 0.5mm))\n"
        ")\n"
        "\n"
        "# TODO: Hole to board edge ≥ 1 mm. Min. board size 10 × 10 mm\n"
        '(rule "Hole (castellated) diameter"\n'
        "\t(layer outer)\n"
        "\t(condition \"A.Type == 'pad' && A.Fabrication_Property == 'Castellated pad'\")\n"
        "\t(constraint hole_size (min 0.6mm))\n"
        ")\n"
        "\n"
        '# JLCPCB: "Via diameter should be 0.1mm(0.15mm preferred) larger than Via hole size"'
        "(illustration shows diameters for both dimensions)\n"
        '# JLCPCB: PTH: "The annular ring size will be enlarged to 0.15mm in production."\n'
        '(rule "Annular ring width (via and PTH)"\n'
        "\t(layer outer)\n"
        '\t(condition "A.isPlated()")\n'
        "\t(constraint annular_width (min 0.075mm))\n"
        ")\n"
        "\n"
        '(rule "Clearance: hole to hole (perimeter), different nets"\n'
        "\t(layer outer)\n"
        '\t(condition "A.Net != B.Net")\n'
        "\t(constraint hole_to_hole (min 0.5mm))\n"
        ")\n"
        "\n"
        '(rule "Clearance: hole to hole (perimeter), same net"\n'
        "\t(layer outer)\n"
        '\t(condition "A.Net == B.Net")\n'
        "\t(constraint hole_to_hole (min 0.254mm))\n"
        ")\n"
        "\n"
        '(rule "Clearance: track to NPTH hole (perimeter)"\n'
        "#\t(condition \"A.Pad_Type == 'NPTH, mechanical' && B.Type == 'track' && A.Net != B.Net\")\n"
        "\t(condition \"!A.isPlated() && B.Type == 'track' && A.Net != B.Net\")\n"
        "\t(constraint hole_clearance (min 0.254mm))\n"
        ")\n"
        "\n"
        '(rule "Clearance: track to PTH hole perimeter"\n'
        "\t(condition \"A.isPlated() && B.Type == 'track' && A.Net != B.Net\")\n"
        "\t(constraint hole_clearance (min 0.33mm))\n"
        ")\n"
        "\n"
        '# TODO: try combining with rule "Clearance: PTH to track, different nets"\n'
        '(rule "Clearance: track to pad"\n'
        "\t(condition \"A.Type == 'pad' && B.Type == 'track' &&  A.Net != B.Net\")\n"
        "\t(constraint clearance (min 0.2mm))\n"
        ")\n"
        "\n"
        '(rule "Clearance: pad/via to pad/via"\n'
        "\t(layer outer)\n"
        "#\t(condition \"(A.Type == 'Pad' || A.Type == 'Via') && "
        "(B.Type == 'Pad' || B.Type == 'Via') && A.Net != B.Net\")\t\n"
        '\t(condition "A.isPlated() && B.isPlated() && A.Net != B.Net")\n'
        "\t(constraint clearance (min 0.127mm))\n"
        ")",
    }
]
