"""
Endpoints for kicad custom design rules.
SPDX-License-Identifier: EUPL-1.2
"""

from typing import Annotated

from fastapi import APIRouter, HTTPException, Query, Response, status
from fastapi.responses import HTMLResponse

from app.config import CONFIG
from app.db import models
from app.db.queries import rule_set as rules_queries
from app.routers.types import DBSession
from app.schema.rule_set import RuleSetSchema
from app.services.auth import CurrentUser, OptionalCurrentUser
from app.services.tasks import AssetNotFound, get_asset_path

router = APIRouter(prefix="/rules", tags=["rules"])


@router.get("/pages")
async def pages(
    actor: OptionalCurrentUser,
    session: DBSession,
    page: Annotated[int, Query(ge=0, le=1000, format="int32")] = 0,
    limit: Annotated[int, Query(ge=1, le=50, format="int32")] = CONFIG.results_per_page,
    seed: Annotated[int, Query(ge=0, le=1000, format="int32")] = CONFIG.seed,
) -> list[RuleSetSchema]:
    """
    Random consistent pagination.
    \nReturn a list of  `RuleSet` in a random order
    but the order is constant for a given seed.
    """
    return [
        RuleSetSchema.from_orm(module)
        for module in await rules_queries.pagination(actor, page, limit, seed, session)
    ]


@router.get("/search")
async def search_rules(
    actor: OptionalCurrentUser,
    query: Annotated[str, Query(alias="q")],
    session: DBSession,
) -> list[RuleSetSchema]:
    """Full text search"""
    return [
        RuleSetSchema.from_orm(module)
        for module in await rules_queries.full_text_search(actor, query, session)
    ]


@router.get(
    "/{username}/{rule_set_name:path}/readme",
    response_class=HTMLResponse,
    responses={404: {"description": "RueleSet or readme not found"}},
)
async def get_rule_set_readme(
    username: str,
    rule_set_name: str,
    actor: OptionalCurrentUser,
    session: DBSession,
) -> str:
    """Get rules readme."""
    try:
        rules = await rules_queries.get_rule_set_by_name(
            username, rule_set_name, actor, session
        )
        rendered_readme_path = get_asset_path(
            rules.repository.location,
            rules.name,
            "readme.html",
            raise_if_not_exist=True,
        )
        return rendered_readme_path.read_text()
    except (models.ModuleNotFound, AssetNotFound) as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(e)) from e


@router.get(
    "/{username}/{rule_set_name:path}",
    responses={404: {"description": "Rules not found"}},
)
async def get_rules_by_name(
    username: str,
    rule_set_name: str,
    actor: OptionalCurrentUser,
    session: DBSession,
) -> RuleSetSchema:
    """Get rules by name."""
    try:
        rules = await rules_queries.get_rule_set_by_name(
            username, rule_set_name, actor, session
        )
    except models.RuleSetNotFound:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    return RuleSetSchema.from_orm(rules)


@router.delete(
    "/{username}/{rule_set_name:path}",
    responses={
        404: {"description": "Rules not found"},
        204: {"description": "Rules deleted"},
    },
)
async def delete_rules(
    username: str,
    rule_set_name: str,
    actor: CurrentUser,
    session: DBSession,
) -> Response:
    """Delete rules by name."""
    try:
        await rules_queries.delete_rule_set(username, rule_set_name, actor, session)
    except models.RuleSetNotFound:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    return Response(status_code=status.HTTP_204_NO_CONTENT)
