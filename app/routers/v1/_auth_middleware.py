"""
This endpoint is not used in the app, but it is used in the tests.
SPDX-License-Identifier: EUPL-1.2
"""

from fastapi import APIRouter, status

from app.db import models
from app.routers.types import DBSession
from app.schema.user import UserSchema
from app.services.auth import CurrentUser

router = APIRouter(prefix="/auth_middleware", tags=["auth_middleware"])


@router.get("/current_user")
async def current_user(actor: CurrentUser) -> UserSchema:
    return UserSchema.from_orm(actor)


@router.post("/new_user", status_code=status.HTTP_201_CREATED)
async def add_test_user(params: UserSchema, session: DBSession) -> UserSchema:
    """Add a test user."""
    user = models.User(
        id=params.id,
        subject=params.subject,
        displayname=params.displayname,
        groups=params.groups,
        roles=params.roles,
    )
    session.add(user)
    await session.commit()

    return UserSchema.from_orm(user)
