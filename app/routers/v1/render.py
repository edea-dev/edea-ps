"""
Endpoint for rendering KiCad schematics and PCBs as an SVG.
SPDX-License-Identifier: EUPL-1.2
"""

import zipfile
from pathlib import Path
from typing import Any, Self, Type

from edea_draw import ThemeName
from fastapi import APIRouter, HTTPException, UploadFile, status
from fastapi.responses import FileResponse, HTMLResponse

from app.config import CONFIG
from app.db import models, queries
from app.routers.types import DBSession
from app.services.auth import CurrentUser
from app.services.tasks import (
    get_task_return_value,
    render_single_kicad_file,
    repositories_path,
)

router = APIRouter(prefix="/render", tags=["render"])


class AllowedFile(UploadFile):  # pylint: disable=too-few-public-methods
    """UploadFile that only allows KiCad schematic and pcb files."""

    @classmethod
    def validate(cls: Type[Self], v: Any) -> Any:
        """Check if file is a KiCad schematic or pcb file."""
        if v.filename.split(".")[-1] not in ("kicad_sch", "kicad_pcb"):
            raise ValueError(
                "Expected a `kicad_sch` or `kicad_pcb` file, got unexpected file:"
                f" `{v.filename}`."
            )
        return super().validate(v)


async def get_text(file: UploadFile) -> str:
    """Get plain text from kicad_sch file."""
    try:
        text = (await file.read()).decode("utf-8")
    except UnicodeDecodeError as e:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f"Could not decode file as UTF-8: {e}.",
        ) from e
    return text


@router.post("/file", response_class=HTMLResponse)
async def render_file(
    actor: CurrentUser, file: AllowedFile, theme: ThemeName = ThemeName.KICAD_2022
) -> str:
    """Render a single KiCad schematic or pcb file as an svg file."""
    text = await get_text(file)
    task = render_single_kicad_file.kiq(text, theme)
    svg, err = await get_task_return_value(task)

    if err is not None:
        raise err
    if svg is None:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Could not render file: {file.filename}.",
        )

    return svg


@router.get(
    "/{username}/{repo_name}/{module_name:path}",
    summary="Render file(s) in EDeA module",
    responses={
        401: {
            "description": "Trying to render a private module without authorization.",
        },
        404: {
            "description": "Module or file not found.",
        },
    },
)
async def render_module(
    username: str,
    repo_name: str,
    module_name: str,
    actor: CurrentUser,
    session: DBSession,
) -> FileResponse:
    """Render file(s) in a  module as an svg file."""
    print(actor.subject)
    try:
        module = await queries.get_module_by_name(
            username, repo_name, module_name, actor, session
        )
    except models.ModuleNotFound as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(e)) from e

    module_assets_dir = (
        CONFIG.assets_path
        / Path(module.repository.location).relative_to(repositories_path)
        / module.name
    )
    print(module_assets_dir)
    if not module_assets_dir.exists():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)

    archive_path = module_assets_dir.with_suffix(".zip")
    if not archive_path.exists():
        with zipfile.ZipFile(module_assets_dir.with_suffix(".zip"), "w") as archive:
            for svg in module_assets_dir.glob("*.svg"):
                archive.write(svg, svg.name)

    return FileResponse(
        str(archive_path.absolute()),
        media_type="application/x-zip-compressed",
        headers={
            "Content-Disposition": "attachment; filename=rendered.zip",
        },
    )
