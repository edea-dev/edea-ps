"""
Endpoints for module management.
SPDX-License-Identifier: EUPL-1.2
"""

import orjson
from edea.kicad.checker import CheckResult
from fastapi import APIRouter, HTTPException, Response, status
from fastapi.responses import HTMLResponse

from app.db import models
from app.db.models.module import ModuleNotFound
from app.db.queries import module as module_queries
from app.db.queries import repository as repo_queries
from app.routers.types import DBSession
from app.schema.module import (
    CommitSchema,
    ModuleSchema,
    SearchParameter,
    SearchParametricModuleParams,
    SearchResultSchema,
)
from app.services.auth import CurrentUser, OptionalCurrentUser
from app.services.cache import (
    cache_key,
    invalidate_cache_entry,
    invalidate_cache_namespace,
)
from app.services.tasks import AssetNotFound, get_asset_path

router = APIRouter(prefix="/module", tags=["module"])


# @cache(namespace="search")
@router.post("/search")
async def search_module(
    actor: OptionalCurrentUser,
    params: SearchParametricModuleParams,
    session: DBSession,
) -> SearchResultSchema:
    """Search the modules using fulltext search or moudle's parameters.
    \nFulltext earchable fields are `name`, `description`, `metadata`, `short_code`, and `repository.url`.
    If no parameters or a query is given, it works as normal pagination.
    \nReturns the matched modules in a random order the but the order is constant for a given seed.
    """
    parameters = (
        {p.type_: p.values for p in params.parameters} if params.parameters else None
    )
    modules, total_pages = await module_queries.search(
        actor,
        params.query,
        parameters,
        params.page,
        params.limit,
        params.seed,
        session,
    )
    return SearchResultSchema(
        hits=[ModuleSchema.from_orm(module) for module in modules],
        total_pages=total_pages,
    )


@router.get("/search/all-parameters")
async def get_all_parameters(
    actor: OptionalCurrentUser,
    session: DBSession,
) -> list[SearchParameter]:
    "All possible search parameters."
    parameters = await module_queries.get_all_parameters(session)

    return [SearchParameter(type=k, values=v) for k, v in parameters.items()]


@router.get("/{username}/{repo_name}")
async def get_repo_modules(
    username: str,
    repo_name: str,
    actor: OptionalCurrentUser,
    session: DBSession,
) -> list[ModuleSchema]:
    """Get modules in a repository."""
    try:
        modules = await repo_queries.get_repo_modules_by_name(
            username, repo_name, actor, session
        )
    except models.UserNotFound as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(e)) from e
    if len(modules) == 0:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    return [ModuleSchema.from_orm(module) for module in modules]


# @cache(namespace="commits")
@router.get(
    "/{username}/{repo_name}/{module_name:path}/commits",
    responses={404: {"description": "Module not found"}},
)
async def get_commits(
    username: str,
    repo_name: str,
    module_name: str,
    actor: OptionalCurrentUser,
    session: DBSession,
) -> list[CommitSchema]:
    """Get module commits."""
    try:
        module = await module_queries.get_module_by_name(
            username, repo_name, module_name, actor, session
        )
    except ModuleNotFound as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(e)) from e

    return [CommitSchema(**orjson.loads(c)) for c in module.commits]


@router.get(
    "/{username}/{repo_name}/{module_name:path}/readme",
    response_class=HTMLResponse,
    responses={404: {"description": "Module or readme not found"}},
)
async def get_readme(
    username: str,
    repo_name: str,
    module_name: str,
    actor: OptionalCurrentUser,
    session: DBSession,
) -> str:
    """Get module readme."""
    try:
        module = await module_queries.get_module_by_name(
            username, repo_name, module_name, actor, session
        )
        rendered_readme_path = get_asset_path(
            module.repository.location,
            module.name,
            "readme.html",
            raise_if_not_exist=True,
        )
        return rendered_readme_path.read_text()
    except (models.ModuleNotFound, AssetNotFound) as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(e)) from e


@router.get(
    "/{username}/{repo_name}/{module_name:path}/violations",
    responses={404: {"description": "Module or vioaltions report not found"}},
)
async def get_violations(
    username: str,
    repo_name: str,
    module_name: str,
    actor: OptionalCurrentUser,
    session: DBSession,
) -> CheckResult:
    """Get module readme."""
    try:
        module = await module_queries.get_module_by_name(
            username, repo_name, module_name, actor, session
        )
        violations_file_path = get_asset_path(
            module.repository.location,
            module.name,
            "violations.json",
            raise_if_not_exist=True,
        )
        return CheckResult(**orjson.loads(violations_file_path.read_text()))
    except (models.ModuleNotFound, AssetNotFound) as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(e)) from e


@router.get(
    "/{username}/{repo_name}/{module_name:path}",
    responses={404: {"description": "Module not found"}},
)
async def get_module_by_name(
    username: str,
    repo_name: str,
    module_name: str,
    actor: OptionalCurrentUser,
    session: DBSession,
) -> ModuleSchema:
    """Get module by name."""
    try:
        module = await module_queries.get_module_by_name(
            username, repo_name, module_name, actor, session
        )
    except models.ModuleNotFound as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND) from e
    return ModuleSchema.from_orm(module)


@router.delete(
    "/{username}/{repo_name}/{module_name:path}",
    responses={
        404: {"description": "Module not found"},
        204: {"description": "Module deleted"},
    },
)
async def delete_module(
    actor: CurrentUser,
    username: str,
    repo_name: str,
    module_name: str,
    session: DBSession,
) -> Response:
    """Delete module by id."""
    try:
        await module_queries.delete_module(
            username, repo_name, module_name, actor, session
        )
    except models.ModuleNotFound:
        return Response(status_code=status.HTTP_404_NOT_FOUND)
    await invalidate_cache_entry(
        await cache_key(
            lambda: None,
            namespace=":user_module",
            kwargs={"user_id": actor.id},
        )
    )
    await invalidate_cache_namespace("search")
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.get("/slugs")
async def get_slugs(
    actor: OptionalCurrentUser,
    session: DBSession,
) -> list[str]:
    """Get all module slugs.\n Used for generating sitemap and pre-rendering."""
    return list(await module_queries.get_all_slugs(actor, session))
