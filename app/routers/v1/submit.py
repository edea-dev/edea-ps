"""
Endpoints for creating new modules and rules.
SPDX-License-Identifier: EUPL-1.2
"""

import json
import pathlib
from datetime import datetime
from shutil import rmtree
from urllib.parse import urlparse
from uuid import UUID

import yaml
from edea.metadata import Project
from fastapi import APIRouter, HTTPException, WebSocket, status
from fastapi.websockets import WebSocketState
from pydantic import BaseModel, Field, HttpUrl, ValidationError, validator

from app.config import CONFIG
from app.db import models
from app.db.queries import category as category_queries
from app.db.queries import repository as repository_queries
from app.routers.types import DBSession
from app.schema.module import CommitSchema, ModuleSchema
from app.schema.rule_set import RuleSetSchema
from app.services.auth import CurrentUser
from app.services.repository import (
    GitCommandError,
    RepositoryService,
    get_clone_location,
    normalize_url,
)
from app.services.tasks import (
    create_and_wait_modules_tasks,
    create_and_wait_rule_sets_tasks,
)

router = APIRouter(tags=["module", "rules"])


class NewModuleParams(BaseModel):
    """Endpoint parameters for creating a new module."""

    repo_url: HttpUrl = Field(
        description="github or gitlab URL of a repository containing the module or ruleset",
        examples=[
            "https://gitlab.com/edea-dev/test-modules.git",
            "https://gitlab.com/edea-dev/test-rules",
        ],
    )
    private: bool = Field(
        False,
        description="whether the module or ruleset should be private on EDeA server",
    )
    access_token: str | None = Field(
        description="oauth2 access token for private repos"
    )

    @validator("repo_url")
    def is_valid_url(cls, url: HttpUrl) -> HttpUrl:
        """validate that repo_url is a valid github. gitlab, gitea url"""
        if url.path is None:
            raise ValueError("Invalid url")
        if url.scheme != "https":
            raise ValueError("Only https is supported!")
        if url.host not in CONFIG.supported_git_hosts:
            raise ValueError(
                "Unsupported git host, only"
                f" {', '.join(CONFIG.supported_git_hosts)} are supported!"
            )
        return url


@router.post(
    "/submit", status_code=201, responses={409: {"description": "Already exists"}}
)
async def submit_module_or_rules(
    actor: CurrentUser,
    params: NewModuleParams,
    session: DBSession,
) -> list[ModuleSchema | RuleSetSchema]:
    """Create modules and rules from a git repository."""
    noramized_url = normalize_url(params.repo_url)
    location = get_clone_location(noramized_url, actor)
    db_repo = models.Repository(
        name=urlparse(noramized_url).path.split("/")[-1],
        user_id=actor.id,
        url=noramized_url,
        type="git",
        location=str(location),
    )
    session.add(db_repo)
    # flush to make sure that there are no errors before cloning
    await session.flush()

    if params.access_token:
        clone_url = params.repo_url.replace(
            "https://", f"https://oauth2:{params.access_token}@"
        )
    else:
        clone_url = params.repo_url

    websocket = open_sockets.get(actor.displayname)

    try:
        await notify_client_msgs(websocket, "Cloning repository")
        repo_service = await RepositoryService.clone_from(clone_url, location)
        print(f"Cloned repository from {clone_url} to {location}")
    except GitCommandError as e:
        await notify_client_msgs(websocket, "Failed to clone repository", terminal=True)
        await session.rollback()
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f"failed to clone repository at {params.repo_url}",
        ) from e

    await notify_client_msgs(websocket, "Cloned repository")
    try:
        await notify_client_msgs(websocket, "Creating rules from repository")
        for rules_config in repo_service.iter_rules_config():
            rules_file = repo_service.find_file(f"{rules_config.dir}/**/*.kicad_dru")
            rules = models.RuleSet(
                user_id=actor.id,
                name=rules_config.name,
                repo_id=db_repo.id,
                private=params.private,
                description=rules_config.description,
                readme_path=rules_config.readme,
                body=rules_file.read_text(),
            )
            session.add(rules)

        await notify_client_msgs(websocket, "Creating modules from repository")

        for module_config in repo_service.iter_modules_config():
            parameters = [
                models.ModuleParameter(
                    type_=type_,
                    value=value,
                )
                for type_, value in module_config.params.items()
            ]

            session.add_all(parameters)

            category = await category_queries.get_category_by_name(
                module_config.category, session
            )

            project_path = repo_service.find_file(
                f"{module_config.dir}/*.kicad_pro",
            )
            commits = [
                CommitSchema(ref=c.hexsha, message=c.message).json()
                for c in RepositoryService(location).iter_commits()
                # filter commits that either touch the module files or the edea.yml file
                if any(
                    pathlib.Path(name).is_relative_to(module_config.dir)  # type: ignore
                    or name == "edea.yml"
                    for name in c.stats.files.keys()
                )
                # only show commits that are newer than 2020-01-01
                and c.committed_datetime.date()
                > datetime.fromisoformat("2020-01-01").date()
            ]

            module = models.Module(
                user_id=actor.id,
                name=module_config.dir,
                metadata_=json.loads(Project(str(project_path)).metadata.json()),
                commits=commits,
                repo_id=db_repo.id,
                private=params.private,
                parameters=parameters,
                description=module_config.description,
                category_id=category.id,
                readme_path=module_config.readme,
            )
            session.add(module)
    except (FileNotFoundError, yaml.YAMLError, ValidationError) as e:
        await notify_client_msgs(
            websocket, "Failed to create modules from repository", terminal=True
        )
        await session.rollback()
        rmtree(location)
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=str(e),
        ) from e

    repo_modules = await repository_queries.get_repo_modules_by_id(
        UUID(str(db_repo.id)), actor, session
    )
    repo_rules = await repository_queries.get_repo_rule_sets_by_id(
        UUID(db_repo.id), actor, session
    )

    serialized_modules = [ModuleSchema.from_orm(module) for module in repo_modules]
    serialized_rules = [RuleSetSchema.from_orm(module) for module in repo_rules]

    try:
        await notify_client_msgs(websocket, "Processing modules")
        await create_and_wait_modules_tasks(serialized_modules, db_repo.location)
        await notify_client_msgs(websocket, "Processing rules")
        await create_and_wait_rule_sets_tasks(serialized_rules, str(location))
        await notify_client_msgs(websocket, "Done 🎉", terminal=True)
    except Exception as e:
        await notify_client_msgs(websocket, "Failed to process modules", terminal=True)
        await session.rollback()
        rmtree(location)
        raise e from e
    await session.commit()

    return serialized_modules + serialized_rules


open_sockets: dict[str, WebSocket] = {}


async def notify_client_msgs(
    socket: WebSocket | None, updates: list[str] | str, terminal: bool = False
) -> None:
    if not socket:
        return

    if isinstance(updates, str):
        updates = [updates]

    for msg in updates:
        await socket.send_text(msg)
        if terminal and socket.application_state == WebSocketState.CONNECTED:
            await socket.close()


@router.websocket("/submit/{displayname}/updates")
async def updates_websocket(websocket: WebSocket, displayname: str) -> None:
    await websocket.accept()
    print("websocket accepted")
    # TODO: replace this when auth is implemented
    open_sockets[displayname] = websocket

    async for _ in websocket.iter_text():
        # keeping the connection open until the client closes it
        ...
