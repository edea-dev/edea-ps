"""
EDD API v1.
SPDX-License-Identifier: EUPL-1.2
"""

from fastapi import APIRouter

from app.config import CONFIG

from ._auth_middleware import router as _auth_middleware_router
from .module import router as module_router
from .render import router as render_router
from .rule_set import router as rules_router
from .submit import router as submit_router
from .user import router as user_router

router = APIRouter(prefix="/v1")
router.include_router(module_router)
router.include_router(render_router)
router.include_router(rules_router)
router.include_router(submit_router)
router.include_router(user_router)

if CONFIG.is_testing:
    router.include_router(_auth_middleware_router)

__all__ = ("router",)
