"""
Endpoints for user management.
SPDX-License-Identifier: EUPL-1.2
"""

from urllib.parse import urlparse

import httpx
from authlib.integrations.starlette_client import OAuth  # type: ignore
from fastapi import APIRouter, HTTPException, Request, Response, status
from fastapi.responses import RedirectResponse
from sqlalchemy.ext.asyncio import AsyncSession

from app.config import CONFIG
from app.db import models
from app.db.queries import user as user_queries
from app.routers.types import DBSession
from app.schema.user import UserSchema
from app.services.auth import CurrentUser, OptionalCurrentUser

from .module import ModuleSchema

router = APIRouter(prefix="/user", tags=["user"])


async def _get_user_modules(
    username: str, actor: models.User | None, session: AsyncSession
) -> list[ModuleSchema]:
    """Get public modules for a user."""
    try:
        modules = await user_queries.get_user_modules(username, actor, session)
    except (models.UserNotFound, models.ModuleNotFound) as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(e)) from e
    return [ModuleSchema.from_orm(module) for module in modules]


@router.get("/{username}/modules", responses={404: {"description": "User not found"}})
async def get_user_modules(
    username: str,
    actor: OptionalCurrentUser,
    session: DBSession,
) -> list[ModuleSchema]:
    """Get public modules for a user."""
    return await _get_user_modules(username, actor, session)


@router.get("/modules")
async def get_current_user_modules(
    actor: CurrentUser, session: DBSession
) -> list[ModuleSchema]:
    """Get all modules for the current user."""
    return await _get_user_modules(actor.displayname, actor, session)


oauth = OAuth()
provider = "dex"
oauth.register(
    name=provider,
    client_id=CONFIG.DEX_CLIENT_ID,
    server_metadata_url=CONFIG.DEX_SERVER_METADATA_URL,
    client_secret=CONFIG.session_secret,
    client_kwargs={"scope": CONFIG.DEX_SCOPE},
)


@router.get("/login", tags=["auth"])
async def login(request: Request, came_from: str = "") -> RedirectResponse:
    """Redirect to OIDC provider for login."""
    prov = getattr(oauth, provider)
    try:
        resp = await prov.authorize_redirect(request, f"{CONFIG.base_url}/v1/user/auth")
    except httpx.ConnectError as e:
        print(f"{CONFIG.base_url}/v1/user/auth")
        raise HTTPException(
            status_code=500,
            detail=f"could not connect to configured OIDC backend: {e}",
        ) from e
    return resp


@router.get("/auth", tags=["auth"])
async def auth(request: Request) -> RedirectResponse:
    """Callback from OIDC provider."""
    prov = getattr(oauth, provider)
    token = await prov.authorize_access_token(request)

    if user := token.get("userinfo"):
        request.session["user"] = dict(user)
    else:
        raise NotImplementedError("")
    return RedirectResponse(url=CONFIG.frontend_url)


@router.get("/logout", tags=["auth"])
async def logout(request: Request, response: Response) -> RedirectResponse:
    """Logout the user."""
    request.session.pop("user", None)
    response.delete_cookie(
        "session", domain=f".{urlparse(CONFIG.frontend_url).hostname}"
    )
    return RedirectResponse(url=CONFIG.frontend_url)


@router.get("/self", tags=["auth"])
async def get_self(actor: CurrentUser) -> UserSchema:
    """Get the current user."""
    return UserSchema.from_orm(actor)
