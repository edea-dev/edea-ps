"""Some common types for FastAP endpoints."""

from typing import Annotated
from uuid import UUID, uuid4

from fastapi import Depends, Path
from fastapi.openapi.models import Example
from sqlalchemy.ext.asyncio import AsyncSession

from app.db.connection import get_db_session

uuid = uuid4()
UserId = Annotated[
    UUID,
    Path(
        openapi_examples={
            "1": Example(
                summary="user id example",
                description="user id",
                value=uuid,
            )
        }
    ),
]
ModuleId = Annotated[
    UUID,
    Path(
        openapi_examples={
            "1": Example(
                summary="module id example",
                description="module id",
                value=uuid,
            ),
        }
    ),
]

RuleSetId = Annotated[
    UUID,
    Path(
        openapi_examples={
            "1": Example(
                summary="rules id example",
                description="rules id",
                value=uuid,
            ),
        }
    ),
]


DBSession = Annotated[AsyncSession, Depends(get_db_session)]
