from pydantic import BaseModel

from app.routers import examples
from app.schema.module import RepositorySchema
from app.schema.user import UserSchema


class RuleSetSchema(BaseModel):
    """EDeA custom desing rules."""

    user: UserSchema
    short_code: str | None
    private: bool
    repository: RepositorySchema
    name: str
    description: str | None
    readme_path: str | None
    body: str

    class Config:
        # pylint: disable=missing-class-docstring
        orm_mode = True
        allow_population_by_field_name = True
        exclude = [
            "user",
            "created_at",
            "updated_at",
            "deleted_at",
        ]
        schema_extra = {"examples": examples.v1_rules}
