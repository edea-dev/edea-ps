from pydantic import BaseModel, Field


class UserSchema(BaseModel):
    """EDeA user."""

    id: str
    subject: str = Field(..., max_length=255)
    displayname: str
    groups: list[str]
    roles: list[str]

    class Config:  # pylint: disable=missing-class-docstring
        orm_mode = True
