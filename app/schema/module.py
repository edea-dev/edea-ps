from typing import Annotated, Any

from edea.metadata import EdeaModuleMetadata
from pydantic import BaseModel, Field, validator

from app.config import CONFIG
from app.routers import examples
from app.schema.user import UserSchema


class ModuleParameterSchema(BaseModel):
    """Module parameter."""

    type_: str = Field(alias="type")
    value: str

    class Config:
        # pylint: disable=missing-class-docstring
        orm_mode = True
        allow_population_by_field_name = True
        exclude = ["modules", "deleted_at", "created_at"]
        schema_extra = {"examples": [{"type": "v_in_min", "value": "4"}]}


class RepositorySchema(BaseModel):
    """EDeA repository."""

    url: str
    name: str

    class Config:
        # pylint: disable=missing-class-docstring
        orm_mode = True
        allow_population_by_field_name = True


class EdeaModuleMetadataSchema(EdeaModuleMetadata):
    count_copper_layer: Annotated[int | None, Field(ge=0, le=24, format="int32")] = None
    count_unique_part: Annotated[int | None, Field(ge=0, le=10000, format="int32")] = (
        None
    )
    count_part: Annotated[int | None, Field(ge=0, le=1000, format="int32")] = None
    sheets: Annotated[int | None, Field(ge=0, le=200, format="int32")] = None


class ModuleSchema(BaseModel):
    """EDeA module."""

    id: str
    user: UserSchema
    short_code: str | None
    private: bool
    repository: RepositorySchema
    name: str
    description: str | None
    category_id: str | None
    metadata_: EdeaModuleMetadataSchema
    readme_path: str | None
    parameters: list[ModuleParameterSchema]

    class Config:
        # pylint: disable=missing-class-docstring
        orm_mode = True
        allow_population_by_field_name = True
        exclude = [
            "user",
            "parameters.modules",
            "created_at",
            "updated_at",
            "deleted_at",
        ]
        schema_extra = {"examples": examples.v1_modules}

    if CONFIG.is_testing:
        # To ease testing, make sure the parameters are sorted.
        @validator("parameters")
        def sort_parameters(cls, parameters: list[ModuleParameterSchema]) -> Any:
            return sorted(parameters, key=lambda p: p.type_)


class CommitSchema(BaseModel):
    """A single git commit"""

    ref: str
    message: str | bytes


class SearchResultSchema(BaseModel):
    hits: list[ModuleSchema]
    total_pages: Annotated[int, Field(ge=0, le=1000, format="int32")]


class SearchParameter(BaseModel):
    type_: str = Field(alias="type")
    values: list[str]

    class Config:
        # pylint: disable=missing-class-docstring
        schema_extra = {"examples": [{"value": examples.v1_search_parameter}]}


class SearchParametricModuleParams(BaseModel):
    """Query body for parametric search."""

    query: str = ""
    parameters: list[SearchParameter] | None = Field(
        examples=[examples.v1_search_parameters]
    )
    page: Annotated[int, Field(ge=0, le=1000, format="int32")] = 0
    limit: Annotated[int, Field(ge=1, le=50, format="int32")] = CONFIG.results_per_page
    seed: Annotated[int, Field(ge=0, le=1000, format="int32")] = CONFIG.seed
