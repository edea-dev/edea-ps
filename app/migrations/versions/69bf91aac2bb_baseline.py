"""baseline

Revision ID: 69bf91aac2bb
Revises: 
Create Date: 2024-09-08 00:47:24.807846

"""

from typing import Sequence, Union

# revision identifiers, used by Alembic.
revision: str = "69bf91aac2bb"
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
