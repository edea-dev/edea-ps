"""
API caching using fastapi-cache2[libsql]
SPDX-License-Identifier: EUPL-1.2
"""

import hashlib
from typing import Annotated, Any, Callable, Optional, get_args, get_origin

from fastapi.datastructures import UploadFile
from fastapi.dependencies.utils import get_typed_signature
from fastapi.requests import Request
from fastapi_cache import FastAPICache, decorator
from fastapi_cache.backends.libsql import LibsqlBackend
from fastapi_cache.decorator import cache  # noqa: F401
from sqlalchemy.ext.asyncio import AsyncSession

from app.config import CONFIG
from app.services.auth import CurrentUser, OptionalCurrentUser


async def setup() -> None:
    """Setup fastapi-cache with sqlite"""
    # pylint: disable=protected-access, redefined-outer-name
    libsql_url = CONFIG.db_url.replace("sqlite+aiosqlite:///", "file:")
    cache = LibsqlBackend(libsql_url=libsql_url, table_name="api_cache")  # noqa: F811
    # monkey patch the cache decorator to allow POST requests to be cached
    decorator._uncacheable = __allow_post_method_caching
    await cache.create_and_flush()
    FastAPICache.init(
        cache,
        key_builder=cache_key,
        enable=CONFIG.enable_cache,
        expire=CONFIG.cache_expire_s,
    )


async def cache_key(
    func: Callable[..., Any],
    namespace: str = "",
    *,
    kwargs: dict[str, Any],
    **kw: Any,
) -> str:
    if len(kw.get("args", [])) > 0:
        raise NotImplementedError("args aren't handled yet")

    for param in get_typed_signature(func).parameters.values():
        if get_origin(param.annotation) is Annotated:
            cls = get_args(param.annotation)[0]
        else:
            cls = param.annotation

        if cls is AsyncSession:
            # remove the AsyncSession from the cache key
            kwargs.pop(param.name, None)
        elif cls in [CurrentUser, OptionalCurrentUser]:
            kwargs[param.name] = kwargs.get(param.name, None)
        elif issubclass(param.annotation, UploadFile):
            # hash the file contents instead of the file object
            file = kwargs[param.name]
            kwargs[param.name] = hashlib.blake2b(await file.read()).hexdigest()
            await file.seek(0)

    for k, v in kwargs.items():  # pylint: disable=invalid-name
        # convert all values to strings
        # for example, UUIDs are converted to strings before hashing
        # because has(UUID(some_uuid)) != shas(tr(some_uuid)
        kwargs[k] = str(v)

    key = hashlib.blake2b(f"{kwargs}".encode(), digest_size=32).hexdigest()
    return f"{namespace}:{key}"


async def invalidate_cache_entry(key: str) -> int:
    """Invalidate a cache entry by key"""
    return await FastAPICache.clear(namespace=None, key=key)


async def invalidate_cache_namespace(namespace: str | None = None) -> int:
    """Invalidate all cache entries in a namespace"""
    return await FastAPICache.clear(namespace=namespace, key=None)


def __allow_post_method_caching(request: Optional[Request]) -> bool:
    """Determine if this request should not be cached

    Returns true if:
    - Caching has been disabled globally
    - This is not a GET request
    - The request has a Cache-Control header with a value of "no-store" or "no-cache"

    """
    if not FastAPICache.get_enable():
        return True
    if request is None:
        return False
    if request.method not in ("GET", "POST"):
        return True
    return request.headers.get("Cache-Control") in ("no-store", "no-cache")
