import asyncio
import copyreg
import itertools
import logging
import os
from pathlib import Path
from typing import Any, Coroutine, Optional, TypeVar
from uuid import UUID

try:
    import pyvips  # type: ignore

    vips_logger = logging.getLogger("pyvips")
    vips_logger.setLevel(logging.WARNING)
except OSError:
    pyvips = None
import taskiq_fastapi  # type: ignore
from asyncer import asyncify
from edea.kicad.checker import check
from edea.kicad.design_rules import Severity
from edea_draw import ThemeName, draw_svg_from_file_content
from fastapi import HTTPException, status
from more_itertools import chunked
from taskiq import AsyncResultBackend, AsyncTaskiqTask, TaskiqScheduler
from taskiq.schedule_sources import LabelScheduleSource
from taskiq_redis import ListQueueBroker, RedisAsyncResultBackend

from app.config import CONFIG
from app.db import connection, queries
from app.db.models.user import SUDO
from app.db.queries.repository import get_repo_modules_by_id
from app.db.queries.rule_set import get_repo_rule_sets
from app.schema.module import ModuleSchema
from app.schema.rule_set import RuleSetSchema
from app.services._readme_links import render_markdown
from app.services.repository import RepositoryService

redis_async_result: AsyncResultBackend[Any] = RedisAsyncResultBackend(
    redis_url=CONFIG.redis_broker_url,
    result_ex_time=5 * 60,  # 5 minutes
)

broker = ListQueueBroker(url=CONFIG.redis_broker_url)
broker.with_result_backend(redis_async_result)

scheduler = TaskiqScheduler(broker=broker, sources=[LabelScheduleSource(broker)])

taskiq_fastapi.init(broker, "app.main:app")
logger = logging.getLogger("taskiq.process-manager")

assets_path = CONFIG.storage_path.joinpath("assets")
repositories_path = CONFIG.storage_path.joinpath("repositories")

if not assets_path.exists():
    os.makedirs(assets_path, exist_ok=True)
if not repositories_path.exists():
    os.makedirs(repositories_path, exist_ok=True)


async def setup() -> None:
    if not broker.is_worker_process:
        print("Starting broker")
        await broker.startup()


async def teardown() -> None:
    if not broker.is_worker_process:
        print("Stopping broker")
        await broker.shutdown()


TaskResult = TypeVar("TaskResult")


async def get_task_return_value(
    task: Coroutine[Any, Any, AsyncTaskiqTask[TaskResult]]
) -> TaskResult:
    """Unwraps the `TaskResult` by waiting for the the task to complete,
    and return the tasks return value"""
    awaited_task = await task
    if not isinstance(awaited_task, AsyncTaskiqTask):
        return awaited_task

    return (await awaited_task.wait_result()).return_value


async def create_and_wait_modules_tasks(
    modules: list[ModuleSchema], repo_location: str
) -> None:
    """
    :raises HTTPException: if any of the tasks fail
    """
    """Create all the tasks for new modules:
    1. render the main kicad files:
        a. r/file.kicad_pro/file.kicad_sch/
        b. r/file.kicad_pro//file_kicad_pch/
    2. render the readme file
    3. generate violations report
    """
    logger.info(f"Creating tasks for `{repo_location}`")
    for module in modules:
        create_assets_dir(os.path.join(repo_location, module.name))

    pro_files = [
        RepositoryService(repo_location).find_file(f"{module.name}/*.kicad_pro")
        for module in modules
    ]
    module_main_files = itertools.chain.from_iterable(
        (
            project_path.with_suffix(".kicad_sch"),
            project_path.with_suffix(".kicad_pcb"),
        )
        for project_path in pro_files
    )

    tasks = (
        [render_module_file.kiq(str(file_path)) for file_path in module_main_files]
        + [
            render_readme.kiq(
                repo_location, module.name, module.readme_path, module.repository.url
            )
            for module in modules
        ]
        + [generate_violations_report.kiq(str(f)) for f in pro_files]
    )

    for batch in chunked(tasks, 5):
        try:
            values: list[tuple[Optional[str], Optional[HTTPException]]] = (
                await asyncio.gather(*[get_task_return_value(task) for task in batch])
            )
            for _, err in values:
                if err is not None:
                    raise err
        except Exception as e:
            logger.error(f"Processing failed: {e}")
            raise HTTPException(
                status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail="Processing failed",
            ) from e


async def create_and_wait_rule_sets_tasks(
    rule_sets: list[RuleSetSchema], repo_location: str
) -> None:
    """Create all the tasks for new rules:
    1. render the readme file
    """
    logger.info(f"Creating tasks for `{repo_location}`")
    for rules in rule_sets:
        create_assets_dir(os.path.join(repo_location, rules.name))

    tasks = [
        render_readme.kiq(
            repo_location, rule_set.name, rule_set.readme_path, rule_set.repository.url
        )
        for rule_set in rule_sets
    ]

    try:
        for batch in chunked(tasks, 5):
            values: list[tuple[Optional[str], Optional[HTTPException]]] = (
                await asyncio.gather(*[get_task_return_value(task) for task in batch])
            )
            for _, err in values:
                if err is not None:
                    raise err
    except Exception as e:
        logger.error(f"Processing failed: {e}")
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail="Processing failed",
        ) from e


def create_assets_dir(repo_location: str) -> None:
    """Create the corresponding assets directory for a repository"""
    logger.debug(f"Creating assets directory for {repo_location}")
    dir_path = assets_path.joinpath(Path(repo_location).relative_to(repositories_path))
    if not os.path.exists(dir_path):
        os.makedirs(str(dir_path), exist_ok=True)


async_draw_svg_from_file_content = asyncify(draw_svg_from_file_content)


@broker.task("render_module_file")
async def render_module_file(
    file_path: str | Path,
) -> tuple[Optional[str], Optional[HTTPException]]:
    """Render `kicad_sch`, or `kicad_pcb` file in a module"""
    logger.info(f"Rendering {file_path}")
    file_path = Path(file_path)
    try:
        svg_content = await async_draw_svg_from_file_content(
            file_path.read_text("utf-8")
        )
    except (EOFError, ValueError) as e:
        return None, HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=str(e)
        )

    try:
        name = "pcb" if file_path.suffix == ".kicad_pcb" else "schematic"
        svg_file_path = assets_path.joinpath(
            file_path.with_name(name).with_suffix(".svg").relative_to(repositories_path)
        )
        svg_file_path.write_text(svg_content)
        og_img_width = 1200
        if pyvips is not None:
            pyvips.Image.new_from_file(
                str(svg_file_path), dpi=1000
            ).thumbnail_image(  # pyright: ignore
                og_img_width
            ).write_to_file(
                str(svg_file_path.with_suffix(".jpg"))
            )
    except ValueError:
        return None, HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    logger.info(f"Rendered {svg_file_path!s}")
    return str(svg_file_path), None


async_check = asyncify(check)


@broker.task("generate_violations_report")
async def generate_violations_report(
    pro_location: str | Path,
) -> tuple[Optional[str], Optional[HTTPException]]:
    """Generate violations report for a repository"""
    logger.info(f"Generating violations report for {pro_location}")
    pro_location = Path(pro_location)
    report_file_path = (
        assets_path
        / pro_location.relative_to(repositories_path).parent
        / "violations.json"
    )

    try:
        check_result = await async_check(pro_location, level=Severity.warning)
        report_file_path.write_text(check_result.json())
    except (FileNotFoundError, ValueError) as e:
        return None, HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=str(e)
        )
    return str(report_file_path), None


def _find_readme_path(repo_location: str, module_name: str) -> Path | None:
    """A best effort to find a readme file in a repository.
    First search the module path for a readme file, if not found search the repository
    """
    readme_pattern = "[Rr][Ee][Aa][Dd][Mm][Ee].[Mm][Dd]"
    repo_service = RepositoryService(repo_location)
    files = list(repo_service.search_files(f"{module_name}/{readme_pattern}"))

    if len(files) > 0:
        return files[0]

    files = list(repo_service.search_files(readme_pattern))

    if len(files) > 0:
        return files[0]
    return None


async def _read_module_readme(
    repo_location: str, module_name: str, module_readme_path: str | None
) -> str | None:
    """Get module readme."""
    repo_service = RepositoryService(repo_location)
    alternative_path = _find_readme_path(repo_location, module_name)

    if module_readme_path is not None:
        txt = repo_service.read_file_content(f"{module_name}/{module_readme_path}")
        if txt is not None:
            return txt

    if alternative_path is not None:
        txt = repo_service.read_file_content(alternative_path)
        if txt is not None:
            return txt

    return None


def get_asset_path(
    repo_location: str,
    module_name: str,
    asset_name: str,
    *,
    raise_if_not_exist: bool = False,
) -> Path:
    p = (
        assets_path
        / Path(repo_location).relative_to(repositories_path)
        / module_name
        / asset_name
    )

    if raise_if_not_exist and not p.exists():
        raise AssetNotFound(
            f"Could not find a {asset_name} file for module `{repo_location}:{module_name}`"
        )
    return p


@broker.task("render_readme")
async def render_readme(
    repo_location: str, module_name: str, module_readme_path: str | None, repo_url: str
) -> tuple[Optional[str], Optional[HTTPException]]:
    logger.info(f"Rendering readme for {repo_location}:{module_name}")
    readme_content = await _read_module_readme(
        repo_location, module_name, module_readme_path
    )
    if readme_content is None:
        return None, HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f"Could not find a readme file for module `{repo_location}:{module_name}`.",
        )
    rendered_readme_path = get_asset_path(repo_location, module_name, "readme.html")

    try:
        html = await render_markdown(
            readme_content,
            module_name,
            repo_url,
        )
    except Exception as e:
        return None, HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=str(e)
        )

    rendered_readme_path.write_text(html)

    logger.info(
        f"Rendered readme for {repo_location}:{module_name} at {rendered_readme_path!s}"
    )
    return str(rendered_readme_path), None


class AssetNotFound(FileNotFoundError):
    """A readme file could not be found."""


def pickle_http_exception(
    exc: HTTPException,
) -> tuple[type[HTTPException], tuple[int, str, Optional[dict[str, Any]]]]:
    """Taskiq stores the tasks result by pickling it.
    Here wen indicate how `HttpExceptions` should be pickled.
    """
    return HTTPException, (exc.status_code, exc.detail, exc.headers)


copyreg.pickle(HTTPException, pickle_http_exception)


@broker.task("render_single_kicad_file")
async def render_single_kicad_file(
    file_content: str, theme: ThemeName = ThemeName.KICAD_2022
) -> tuple[Optional[str], Optional[HTTPException]]:
    try:
        return (await async_draw_svg_from_file_content(file_content, theme)), None
    except UnicodeDecodeError as e:
        return None, HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f"Could not decode file as UTF-8: {e}.",
        )
    except (EOFError, ValueError) as e:
        return None, HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=str(e)
        )


@broker.task
async def pull_repo_updates(repo_location: str) -> tuple[bool, RuntimeError | None]:
    """Pull repo updates from all its remotes
    :returns: a tuple of (bool, RuntimeError) if the bool is true that indicates there are updates.
    """
    logger.info(f"Pulling updates: `{repo_location}`")
    remote = RepositoryService(repo_location).remotes[0]
    pull_results = remote.pull()

    has_pulled_updates = False
    for pull in pull_results:
        if pull.flags == pull.ERROR or pull.flags == pull.REJECTED:
            return has_pulled_updates, RuntimeError(
                f"Failed to pull: {repo_location}:{pull!s}"
            )
        elif pull.flags == pull.HEAD_UPTODATE:
            logger.info(f"`{repo_location}:{pull!s}` is up to date")
        else:
            logger.info(f"Updated `{repo_location}`")
            has_pulled_updates = True

    return has_pulled_updates, None


@broker.task("schedule_repo_updates_tasks", schedule=[{"cron": CONFIG.pull_repos_cron}])
async def schedule_repo_updates_tasks() -> None:
    """Schedule tasks for pulling repositories updates.
    If a repo has been updated, recreate tasks for its modules.
    """
    session = connection.SessionLocal()
    async with connection.SessionLocal() as session:
        repos = await queries.get_all_repos(session)
        for repo in repos:
            should_re_run_tasks, err = await get_task_return_value(
                pull_repo_updates.kiq(repo.location)
            )
            if err is not None:
                raise err

            if should_re_run_tasks:
                repo_modules = await get_repo_modules_by_id(
                    UUID(repo.id), SUDO, session
                )
                repo_rules = await get_repo_rule_sets(UUID(repo.id), SUDO, session)
                await create_and_wait_modules_tasks(
                    [ModuleSchema.from_orm(module) for module in repo_modules],
                    repo.location,
                )
                await create_and_wait_rule_sets_tasks(
                    [RuleSetSchema.from_orm(rules) for rules in repo_rules],
                    repo.location,
                )
