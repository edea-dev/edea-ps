import logging
import re
import xml.etree.ElementTree as etree
from typing import TYPE_CHECKING, Any, Callable, Literal, TypeAlias
from urllib.parse import ParseResult, urljoin, urlparse, urlunparse

import markdown
from asyncer import asyncify
from markdown import Markdown
from markdown.extensions import Extension
from markdown.inlinepatterns import InlineProcessor

from app.config import CONFIG

logger = logging.getLogger("taskiq.process-manager")


class BaseRewriteProcessor(InlineProcessor):
    def __init__(
        self,
        pattern: str,
        rewrite_fn: Callable[[str, str, str], str],
        repo_url: str,
        module_name: str,
        md: Markdown,
    ):
        super().__init__(pattern, md)
        self.rewrite_fn = rewrite_fn
        self.repo_url = repo_url
        self.module_name = module_name

    def handleMatch(self, m: re.Match[str]) -> str | etree.Element | None:  # type: ignore
        raise NotImplementedError("Subclasses must implement handleMatch method")


class BaseRewriteExtension(Extension):
    def __init__(self, **kwargs: Any):
        self.config = {
            "rewrite_fn": [
                lambda repo_url, module_name, url: url,
                "Function to rewrite URLs",
            ],
            "repo_url": ["", "repo URL"],
            "module_name": ["", "EDeA module name (the dir)"],
        }
        super().__init__(**kwargs)

    def extendMarkdown(self, md: Markdown) -> None:
        raise NotImplementedError("Subclasses must implement extendMarkdown method")


class ImageRewriteProcessor(BaseRewriteProcessor):
    def handleMatch(self, m: re.Match, data: str):  # type: ignore
        img_tag = etree.Element("img")
        img_tag.set(
            "src",
            self.rewrite_fn(self.repo_url, self.module_name, m.group(2)),
        )
        if m.group(1):
            img_tag.set("alt", m.group(1))
        img_tag.set("decoding", "async")
        img_tag.set("loading", "lazy")

        return img_tag, m.start(0), m.end(0)


class ImageRewriteExtension(BaseRewriteExtension):
    def extendMarkdown(self, md: Markdown) -> None:
        rewrite_fn = self.getConfig("rewrite_fn")
        repo_url = self.getConfig("repo_url")
        module_name = self.getConfig("module_name")
        IMAGE_LINK_RE = r"!\[([^\]]*)\]\(([^\)]+)\)"
        md.inlinePatterns.register(
            ImageRewriteProcessor(IMAGE_LINK_RE, rewrite_fn, repo_url, module_name, md),
            "image_rewrite",
            175,
        )


class AnchorRewriteProcessor(BaseRewriteProcessor):
    def handleMatch(self, m: re.Match, data: str):  # type: ignore
        anchor_tag = etree.Element("a")
        anchor_tag.set(
            "href",
            self.rewrite_fn(self.repo_url, self.module_name, m.group(2)),
        )
        if m.group(1):
            anchor_tag.text = m.group(1)
        return anchor_tag, m.start(0), m.end(0)


class AnchorRewriteExtension(BaseRewriteExtension):
    def extendMarkdown(self, md: Markdown) -> None:
        rewrite_fn = self.getConfig("rewrite_fn")
        repo_url = self.getConfig("repo_url")
        module_name = self.getConfig("module_name")
        ANCHOR_LINK_RE = r"\[([^\]]+)\]\(([^\)]+)\)"
        md.inlinePatterns.register(
            AnchorRewriteProcessor(
                ANCHOR_LINK_RE, rewrite_fn, repo_url, module_name, md
            ),
            "anchor_rewrite",
            175,
        )


ABS_URL_PATTHERN = re.compile(
    r"^[a-z0-9]*?://|^(mailto|tel|sms|magnet):", re.IGNORECASE
)


def is_abs_url(uri: str) -> bool:
    return bool(ABS_URL_PATTHERN.match(uri))


def normalize_relative_url(url: str) -> str:
    return url[1:] if url.startswith("/") else url


HOST: TypeAlias = Literal["github.com", "gitlab.com"]


def add_custom_extensions(
    repo_url: str,
    module_name: str,
) -> list[str | Extension]:
    parsed_repo_url = urlparse(repo_url)
    host = parsed_repo_url.hostname

    if TYPE_CHECKING:
        assert host in CONFIG.supported_git_hosts

    def image_rewrite_fn(repo_url: str, module_name: str, url: str) -> str:
        if is_abs_url(url):
            return url

        match host:
            case "gitlab.com":
                return f"{repo_url}/-/raw/HEAD/{urljoin(f'{module_name}/', normalize_relative_url(url))}"
            case "github.com":
                new_hostname = "raw.githubusercontent.com"
                url_path = parsed_repo_url.path.split("/")

                # Avoid modifying GitHub Actions status badges
                is_workflow_path = (
                    url_path[3] in ["workflows", "actions"]
                    if len(url_path) > 3
                    else False
                )

                if not is_workflow_path:
                    # Remove `/blob/` or '/raw/' from the path
                    new_path = "/".join(url_path[:3] + url_path[4:])
                    new_path = urljoin(
                        f"{new_path}/HEAD/{module_name}/", normalize_relative_url(url)
                    )
                    parsed_url = ParseResult(
                        scheme=parsed_repo_url.scheme,
                        netloc=new_hostname,
                        path=new_path,
                        params=parsed_repo_url.params,
                        query=parsed_repo_url.query,
                        fragment=parsed_repo_url.fragment,
                    )

                    return urlunparse(parsed_url)
                return url
            case _:
                raise NotImplementedError(f"Unsupported host: {host}.")

    def anchor_rewrite_fn(repo_url: str, module_name: str, url: str) -> str:
        if is_abs_url(url):
            return url
        return url

    return [
        "mdx_headdown",
        "nl2br",
        "extra",
        ImageRewriteExtension(
            rewrite_fn=image_rewrite_fn,
            repo_url=repo_url,
            module_name=module_name,
        ),
        AnchorRewriteExtension(
            rewrite_fn=anchor_rewrite_fn,
            repo_url=repo_url,
            module_name=module_name,
        ),
    ]


def _render_markdown(md_text: str, module_name: str, repo_url: str) -> str:
    md = markdown.Markdown(
        extensions=add_custom_extensions(module_name=module_name, repo_url=repo_url)
    )

    return md.convert(md_text)


render_markdown = asyncify(_render_markdown)
