"""
Manage edea Module repository.

SPDX-License-Identifier: EUPL-1.2
"""

import os
from functools import cached_property
from glob import glob
from pathlib import Path
from typing import Any, Generator, Mapping, Self, cast
from warnings import warn

from asyncer import asyncify
from git import GitCommandError
from git.repo import Repo
from git.types import CallableProgress, PathLike
from pydantic import BaseModel, Extra, Field
from yaml import YAMLError, safe_load

from app.config import CONFIG
from app.db.models.user import User


class RepositoryService(Repo):
    """Service for interacting with git repositories."""

    @classmethod
    async def clone_from(  # type: ignore[override]
        cls,
        url: PathLike,
        to_path: PathLike,
        progress: CallableProgress | None = None,
        env: Mapping[str, str] | None = None,
        multi_options: list[str] | None = None,
        allow_unsafe_protocols: bool = False,
        allow_unsafe_options: bool = False,
        **kwargs: Any,
    ) -> Self:
        repository = await asyncify(super().clone_from)(
            url,
            to_path,
            progress,
            env,
            multi_options,
            allow_unsafe_protocols,
            allow_unsafe_options,
            **kwargs,
        )
        return cls(repository.working_dir, type(repository.odb))

    def __enter__(self) -> Self:
        return self  # pragma: no cover

    def search_files(
        self, patterns: str | list[str], recursive: bool = False
    ) -> Generator[Path, None, None]:
        if isinstance(patterns, str):
            patterns = [patterns]
        return (
            Path(match)
            for pattern in patterns
            for match in glob(
                os.path.join(self.working_dir, pattern),
                recursive=recursive,
            )
        )

    def find_file(self, pattern: str | list[str], recursive: bool = False) -> Path:
        """Find a single file matching the given pattern."""
        files = list(self.search_files(pattern, recursive=recursive))
        if len(files) == 0:
            raise FileNotFoundError(f"No file matched the `{pattern}` pattern")

        if len(files) > 1:
            warn(
                "Multiple files matched the given pattern\n"
                f"{', '.join(map(str, files[:5]))}",
            )
        return files[0]

    def read_file_content(self, path: Path | str) -> str | None:
        if isinstance(path, Path):
            path = path.relative_to(self.working_dir)
        try:
            return cast(str, self.git.show(f"remotes/origin/HEAD:{path}"))
        except GitCommandError:
            return None

    @cached_property
    def edea_config(self) -> "EDeAConfig":
        return EDeAConfig.from_yaml(self.find_file(["edea.yml", "edea.yaml"]))

    def iter_modules_config(self) -> Generator["ModuleConfig", Any, None]:
        for module_config in self.edea_config.modules:
            yield module_config

    def iter_rules_config(self) -> Generator["RulesConfig", Any, None]:
        for rules_config in self.edea_config.rules:
            yield rules_config


class ModuleConfig(BaseModel):
    """EDeA module configuration defined in edea.yml."""

    name: str
    readme: str | None = None
    description: str | None = None
    category: str | None = None
    dir: str | None = Field(None, alias="directory")
    params: dict[str, str] = {}

    class Config:
        # pylint: disable=missing-class-docstring
        extra = Extra.forbid
        allow_population_by_field_name = True


class RulesConfig(BaseModel):
    """EDeA module configuration defined in edea.yml."""

    name: str
    readme: str | None = None
    description: str | None = None
    dir: str | None = Field(None, alias="directory")

    class Config:
        # pylint: disable=missing-class-docstring
        extra = Extra.forbid
        allow_population_by_field_name = True


class EDeAConfig(BaseModel):
    """EDeA repository configuration defined in edea.yml."""

    name: str
    path: str
    modules: list[ModuleConfig] = []
    rules: list[RulesConfig] = []

    @classmethod
    def from_yaml(cls, path: Path) -> Self:
        data: dict[str, Any] = safe_load(path.read_text())
        modules: dict[str, Any] | None = data.get("modules", None)
        rules: dict[str, Any] | None = data.get("rules", None)
        if modules is None and rules is None:
            raise YAMLError(f"Missing `modules` or `rules` key in `{path}`. ")

        if modules is not None:
            data["modules"] = [
                ModuleConfig(name=module_name, **module_data)
                for module_name, module_data in modules.items()
            ]
        if rules is not None:
            data["rules"] = [
                RulesConfig(name=rules_name, **rules_data)
                for rules_name, rules_data in rules.items()
            ]
        # TODO: make it relative to the repository root
        data["path"] = str(path.parent)

        return cls(**data)

    class Config:
        # pylint: disable=missing-class-docstring
        extra = Extra.forbid


def normalize_url(url: str) -> str:
    """Normalize the given URL."""
    return url.removesuffix(".git")


def get_clone_location(normalized_url: str, actor: User) -> Path:
    """Get the location where the repository should be cloned."""
    canonical_repo_name = normalized_url.replace(
        normalized_url.split("/")[0], actor.displayname
    )

    full_repo_path = CONFIG.storage_path.joinpath("repositories", canonical_repo_name)
    return full_repo_path


__all__ = (
    "RepositoryService",
    "GitCommandError",
    "ModuleConfig",
    "EDeAConfig",
    "normalize_url",
    "get_clone_location",
)
