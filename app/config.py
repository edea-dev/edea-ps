"""
Load and validate environment variables.
SPDX-License-Identifier: EUPL-1.2
"""

import sys
from os import getenv
from pathlib import Path
from pprint import pp
from typing import Final

import certifi
from dotenv import load_dotenv
from pydantic import validator
from pydantic.dataclasses import dataclass

IS_TESTING = "pytest" in sys.modules


@dataclass
class Config:
    """Configuration from environment variables."""

    alembic_db_url: str
    assets_path: Path
    base_url: str
    cache_expire_s: int
    db_url: str
    enable_cache: bool
    frontend_url: str
    gitlab_private_token: str
    is_testing = IS_TESTING
    jwks_url: str
    pull_repos_cron: str
    redis_broker_url: str
    results_per_page: int
    seed = 0
    session_secret: str
    storage_path: Path
    supported_git_hosts: list[str]

    DEX_CLIENT_ID: str
    DEX_SCOPE: str
    DEX_SERVER_METADATA_URL: str

    @validator("gitlab_private_token")
    def validate_gitlab_private_token(cls, value: str) -> str:
        """Make sure the gitlab private token is set if the gitlab host is supported."""
        if not value:
            raise ValueError(
                "GITLAB_PRIVATE_TOKEN is required if gitlab.com is supported"
            )
        return value

    @validator("db_url")
    def testing_db_url(cls, value: str) -> str:  # pragma: no cover
        """Set the test database if we're running tests."""
        if IS_TESTING:
            return "sqlite+aiosqlite:///app/tests/test.sqlite"
        return value

    @validator("enable_cache")
    def testing_enable_cache(cls, value: bool) -> bool:  # pragma: no cover
        """Disable cache if we're running tests."""
        if IS_TESTING:
            return False
        return value


def add_local_certs() -> None:
    caddy_local_cert = getenv("CADDY_LOCAL_CERT_PATH")
    if caddy_local_cert is None:
        return
    with open(caddy_local_cert, "rb") as f1, open(certifi.where(), "wb") as f2:
        f2.write(f1.read())


load_dotenv()
add_local_certs()

_storage_path = Path(getenv("STORAGE_PATH", "/storage"))
# types are ignored because getenv returns a string | None and pydantic handles the conversion
CONFIG: Final = Config(
    alembic_db_url=getenv("ALEMBIC_DB_URL"),  # type: ignore
    assets_path=_storage_path.joinpath("assets"),
    base_url=getenv("VITE_BACKEND_URL"),  # type: ignore
    cache_expire_s=getenv("CACHE_EXPIRE_S", f"{60 * 60 * 24}"),  # type: ignore
    db_url=getenv("DB_URL"),  # type: ignore
    enable_cache=getenv("ENABLE_CACHE", "true"),  # type: ignore
    frontend_url=getenv("VITE_FRONTEND_URL", "https://edea.test"),
    gitlab_private_token=getenv("GITLAB_PRIVATE_TOKEN", ""),
    jwks_url=getenv("JWKS_URL"),  # type: ignore
    # Leaving the cron  expression for a every minute here for devs "*/1 * * * *"
    pull_repos_cron=getenv("PULL_REPOS_CRON", "0 0 */1 * *"),
    redis_broker_url=getenv("REDIS_BROKER_URL", ""),
    results_per_page=getenv("VITE_RESULTS_PER_PAGE", "50"),  # type: ignore
    session_secret=getenv("OIDC_SESSION_SECRET"),  # type: ignore
    storage_path=_storage_path,
    supported_git_hosts=getenv("SUPPORTED_GIT_HOSTS", "github.com").split(","),
    #
    DEX_CLIENT_ID=getenv("DEX_CLIENT_ID", "edea-ps"),
    DEX_SCOPE=getenv("DEX_SCOPE", "openid profile groups"),
    DEX_SERVER_METADATA_URL=getenv(
        "DEX_SERVER_METADATA_URL",
        "https://dex.edea.test/dex/.well-known/openid-configuration",
    ),
)

pp(CONFIG)


__all__ = ["CONFIG"]
