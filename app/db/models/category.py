"""
Module model.
SPDX-License-Identifier: EUPL-1.2
"""

from sqlalchemy.orm import Mapped, mapped_column

from app.db.connection import Base


class Category(Base):
    """EDeA module category."""

    __tablename__ = "categories"
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(nullable=False)
    description: Mapped[str]
