"""
RuleSet model.
SPDX-License-Identifier: EUPL-1.2
"""

from datetime import datetime
from uuid import uuid4

from sqlalchemy import DateTime, UniqueConstraint, func
from sqlalchemy.orm import Mapped, mapped_column

from app.db.connection import Base
from app.db.models.repository import ProvidesRepositoryMixin
from app.db.models.user import ProvidesUserMixin


class RuleSet(
    Base, ProvidesRepositoryMixin, ProvidesUserMixin
):  # pylint: disable=too-few-public-methods
    """EDeA custom design rules."""

    __tablename__ = "rule_sets"

    id: Mapped[str] = mapped_column(primary_key=True, default=lambda: str(uuid4()))
    short_code: Mapped[str] = mapped_column(nullable=True)
    body: Mapped[str] = mapped_column(nullable=False)
    private: Mapped[bool] = mapped_column(default=False)
    name: Mapped[str] = mapped_column(index=True)
    description: Mapped[str] = mapped_column(nullable=True)
    readme_path: Mapped[str] = mapped_column(nullable=True)
    created_at: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=func.now()
    )
    updated_at: Mapped[datetime] = mapped_column(
        DateTime(timezone=True),
        onupdate=func.now(),
        nullable=True,
    )
    deleted_at: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=True, index=True
    )

    __table_args__ = (UniqueConstraint("name", "user_id", name="idx_rule_set_owner"),)


class RuleSetNotFound(Exception):
    """RuleSet not found."""
