"""
Module model.
SPDX-License-Identifier: EUPL-1.2
"""

from datetime import datetime
from typing import Any
from uuid import uuid4

from sqlalchemy import JSON, DateTime, ForeignKey, Text, UniqueConstraint, func
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.db.connection import Base

from .repository import ProvidesRepositoryMixin
from .user import ProvidesUserMixin


class ModuleParameterAssociation(Base):  # pylint: disable=too-few-public-methods
    """EDeA module parameter association."""

    __tablename__ = "modules_to_parameters"

    id: Mapped[int] = mapped_column(primary_key=True)
    module_id: Mapped[str] = mapped_column(Text(length=36), ForeignKey("modules.id"))
    parameter_id: Mapped[str] = mapped_column(
        Text(length=36), ForeignKey("parameters.id")
    )


class Module(
    Base, ProvidesRepositoryMixin, ProvidesUserMixin
):  # pylint: disable=too-few-public-methods
    """EDeA module."""

    __tablename__ = "modules"

    id: Mapped[str] = mapped_column(primary_key=True, default=lambda: str(uuid4()))
    short_code: Mapped[str] = mapped_column(nullable=True)
    private: Mapped[bool] = mapped_column(default=False)
    name: Mapped[str] = mapped_column(index=True)
    description: Mapped[str] = mapped_column(nullable=True)
    readme_path: Mapped[str] = mapped_column(nullable=True)
    category_id: Mapped[int] = mapped_column(ForeignKey("categories.id"))
    commits: Mapped[list[str]] = mapped_column(type_=JSON, nullable=False, default=[])

    metadata_: Mapped[dict[str, Any]] = mapped_column(
        JSON, name="metadata", default=dict
    )
    created_at: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=func.now()
    )
    updated_at: Mapped[datetime] = mapped_column(
        DateTime(timezone=True),
        onupdate=func.now(),
        nullable=True,
    )
    deleted_at: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=True, index=True
    )

    parameters: Mapped[list["ModuleParameter"]] = relationship(
        secondary="modules_to_parameters",
        back_populates="modules",
    )

    __table_args__ = (UniqueConstraint("name", "user_id", name="idx_module_owner"),)


class ModuleParameter(Base):  # pylint: disable=too-few-public-methods
    """EDeA module parameter."""

    __tablename__ = "parameters"

    id: Mapped[str] = mapped_column(primary_key=True, default=lambda: str(uuid4()))
    type_: Mapped[str] = mapped_column(name="type")
    value: Mapped[str]
    created_at: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=func.now()
    )
    updated_at: Mapped[datetime] = mapped_column(
        DateTime(timezone=True),
        onupdate=func.now(),
        nullable=True,
    )
    deleted_at: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=True, index=True
    )

    modules: Mapped[list[Module]] = relationship(
        secondary="modules_to_parameters", back_populates="parameters"
    )


class ModuleNotFound(Exception):
    """Module not found."""
