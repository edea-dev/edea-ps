"""
Repository model
SPDX-License-Identifier: EUPL-1.2
"""

from datetime import datetime
from uuid import uuid4

from sqlalchemy import DateTime, Enum, ForeignKey, UniqueConstraint, func
from sqlalchemy.orm import Mapped, declared_attr, mapped_column, relationship

from app.db.connection import Base

from .user import ProvidesUserMixin


class Repository(Base, ProvidesUserMixin):  # pylint: disable=too-few-public-methods
    """Repository model."""

    __tablename__ = "repositories"

    id: Mapped[str] = mapped_column(primary_key=True, default=lambda: str(uuid4()))
    name: Mapped[str] = mapped_column(nullable=False, index=True)
    url: Mapped[str] = mapped_column(nullable=False, index=True)
    type: Mapped[str] = mapped_column(Enum("git", "other"))
    location: Mapped[str]
    created_at: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=func.now()
    )
    updated_at: Mapped[datetime] = mapped_column(
        DateTime(timezone=True),
        onupdate=func.now(),
        nullable=True,
    )
    deleted_at: Mapped[datetime] = mapped_column(DateTime(timezone=True), nullable=True)

    __table_args__ = (UniqueConstraint("url", "user_id", name="idx_repo_owner"),)


class ProvidesRepositoryMixin:
    """A mixin that add `repository` relationship to a model."""

    repo_id: Mapped[str] = mapped_column(ForeignKey("repositories.id"))

    @declared_attr
    def repository(cls) -> Mapped[Repository]:
        return relationship("Repository", lazy="selectin")
