"""
Models for `users` table
SPDX-License-Identifier: EUPL-1.2
"""

from datetime import datetime
from uuid import uuid4

from sqlalchemy import JSON, DateTime, ForeignKey, func
from sqlalchemy.orm import Mapped, declared_attr, mapped_column, relationship

from app.db.connection import Base


class User(Base):  # pylint: disable=too-few-public-methods
    """User model"""

    __tablename__ = "users"

    id: Mapped[str] = mapped_column(primary_key=True, default=lambda: str(uuid4()))
    subject: Mapped[str] = mapped_column(nullable=False, unique=True)
    displayname: Mapped[str] = mapped_column(nullable=False, unique=True, index=True)
    groups: Mapped[list[str]] = mapped_column(JSON)
    roles: Mapped[list[str]] = mapped_column(JSON)
    disabled: Mapped[bool] = mapped_column(default=False)
    created_at: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=func.now()
    )
    updated_at: Mapped[datetime] = mapped_column(
        DateTime(timezone=True),
        onupdate=func.now(),
        nullable=True,
    )
    deleted_at: Mapped[datetime] = mapped_column(DateTime(timezone=True), nullable=True)


SUDO = User(subject="internal_admin", displayname="internal_admin", roles=["admin"])


class ProvidesUserMixin:
    """A mixin that add `user` relationship to a model."""

    user_id: Mapped[str] = mapped_column(ForeignKey("users.id"))

    @declared_attr
    def user(cls) -> Mapped[User]:
        return relationship("User", lazy="selectin")


class UserNotFound(Exception):
    """User not found"""
