"""
SPDX-License-Identifier: EUPL-1.2
Configure full text search for modules.
"""

import sqlite_utils

from app.config import CONFIG

db = sqlite_utils.Database(CONFIG.db_url.split("///")[1])


def create_fts() -> None:
    """Idempotent creation of full text search tables for modules and rules."""

    if "modules_fts" not in db.table_names():
        db["modules"].enable_fts(
            [
                "id",
                "short_code",
                "name",
                "description",
                "metadata",
                "deleted_at",
            ],
            tokenize="porter",
            create_triggers=True,
        )

    if "rule_sets_fts" not in db.table_names():
        db["rule_sets"].enable_fts(
            [
                "id",
                "short_code",
                # TODO: "repo_url",
                "name",
                "description",
                "deleted_at",
            ],
            tokenize="porter",
            create_triggers=True,
        )
