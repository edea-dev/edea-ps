"""
Queries for the `users` table.
SPDX-License-Identifier: EUPL-1.2
"""

from typing import Sequence

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload

from app.db import models
from app.db.queries.common import has_permission_for


async def get_user_by_name(username: str, session: AsyncSession) -> models.User | None:
    """Get a user by their username."""
    user = await session.execute(
        select(models.User).where(models.User.displayname == username)
    )
    return user.scalar_one_or_none()


async def get_user_modules(
    username: str, actor: models.User | None, session: AsyncSession
) -> Sequence[models.Module]:
    """Get public modules for a user."""
    user = await get_user_by_name(username, session)
    if not user:
        raise models.UserNotFound(f"Could not find user`{username}`")

    modules = (
        (
            await session.execute(
                select(models.Module)
                .where(
                    models.Module.user_id == user.id,
                    has_permission_for(actor, models.Module),
                    models.Module.deleted_at.is_(None),
                )
                .options(selectinload(models.Module.parameters))
            )
        )
        .scalars()
        .all()
    )

    if len(modules) == 0:
        raise models.ModuleNotFound(f"No modules found for user `{username}`")

    return modules
