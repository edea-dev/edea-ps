"""
Queries for the `repositories` table.
SPDX-License-Identifier: EUPL-1.2
"""

from typing import Sequence
from uuid import UUID

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload

from app.db import models
from app.db.models.user import User
from app.db.queries.common import has_permission_for


async def get_repo_modules_by_id(
    id: UUID, user: User | None, session: AsyncSession
) -> Sequence[models.Module]:
    return (
        (
            await session.execute(
                select(models.Module)
                .where(
                    models.Module.repo_id == str(id),
                    has_permission_for(user, models.Module),
                    models.Module.deleted_at.is_(None),
                )
                .options(selectinload(models.Module.parameters))
            )
        )
        .scalars()
        .all()
    )


async def get_repo_modules_by_name(
    username: str, repo_name: str, user: User | None, session: AsyncSession
) -> Sequence[models.Module]:
    return (
        (
            await session.execute(
                select(models.Module)
                .join(models.Repository)
                .where(
                    models.Repository.name == repo_name,
                    models.Repository.user.has(displayname=username),
                    has_permission_for(user, models.Module),
                    models.Module.deleted_at.is_(None),
                )
                .options(selectinload(models.Module.parameters))
            )
        )
        .scalars()
        .all()
    )


async def get_repo_rule_sets_by_id(
    id: UUID, user: User | None, session: AsyncSession
) -> Sequence[models.RuleSet]:
    return (
        (
            await session.execute(
                select(models.RuleSet).where(
                    models.RuleSet.repo_id == str(id),
                    has_permission_for(user, models.RuleSet),
                    models.RuleSet.deleted_at.is_(None),
                )
            )
        )
        .scalars()
        .all()
    )


async def get_repo_rule_sets_by_name(
    username: str, repo_name: str, user: User | None, session: AsyncSession
) -> Sequence[models.RuleSet]:
    return (
        (
            await session.execute(
                select(models.RuleSet)
                .join(models.Repository)
                .where(
                    models.Repository.name == repo_name,
                    models.Repository.user.has(displayname=username),
                    has_permission_for(user, models.RuleSet),
                    models.RuleSet.deleted_at.is_(None),
                )
            )
        )
        .scalars()
        .all()
    )


async def get_all_repos(session: AsyncSession) -> Sequence[models.Repository]:
    return (await session.execute(select(models.Repository))).scalars().all()
