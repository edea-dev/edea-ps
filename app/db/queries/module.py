"""
Queries for the `modules` table.
SPDX-License-Identifier: EUPL-1.2
"""

from datetime import datetime
from typing import Any, Sequence
from uuid import UUID

from sqlalchemy import (
    BinaryExpression,
    False_,
    True_,
    and_,
    false,
    func,
    or_,
    select,
    text,
    true,
)
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload

from app.db.models import (
    Module,
    ModuleNotFound,
    ModuleParameter,
    ModuleParameterAssociation,
    User,
)
from app.db.models.repository import Repository
from app.db.queries.common import has_permission_for


async def get_module_by_id(
    module_id: UUID, user: User | None, session: AsyncSession
) -> Module:
    module = (
        await session.execute(
            select(Module)
            .where(
                Module.id == str(module_id),
                Module.deleted_at.is_(None),
                has_permission_for(user, Module),
            )
            .options(selectinload(Module.user), selectinload(Module.parameters))
        )
    ).scalar_one_or_none()

    if module is None:
        raise ModuleNotFound(f"Could not find module with id `{module_id}`.")
    return module


async def get_module_by_name(
    username: str, repo_name: str, name: str, user: User | None, session: AsyncSession
) -> Module:
    module = (
        await session.execute(
            select(Module)
            .join(User, User.id == Module.user_id)
            .join(Repository, Repository.id == Module.repo_id)
            .where(
                has_permission_for(user, Module),
                Module.name == name,
                Module.deleted_at.is_(None),
                User.displayname == username,
                User.disabled.is_(False),
                Repository.name == repo_name,
                Repository.deleted_at.is_(None),
                Repository.user_id == Module.user_id,
            )
            .options(selectinload(Module.user), selectinload(Module.parameters))
        )
    ).scalar_one_or_none()

    if module is None:
        raise ModuleNotFound(
            f"Could not find module with name `{name}` for user `{username}`."
        )
    return module


async def search(
    user: User | None,
    q: str,
    parameters: dict[str, list[str]] | None,
    page: int,
    limit: int,
    seed: int,
    session: AsyncSession,
) -> tuple[Sequence[Module], int]:
    """Search the modules using fulltext search or module's parameters.
    \nReturns the matched modules in a random order the but the order is constant for a given seed.
    \nIf no parameters or a query is given, it works as normal pagination.
    """
    if page < 0 or limit <= 0:
        return [], 0

    escaped_query = '"' + q.replace('"', '""') + '"'

    if parameters is not None:
        filters: list[BinaryExpression[bool] | True_] = [
            Module.id.in_(
                select(ModuleParameterAssociation.module_id)
                .join(ModuleParameter)
                .where(
                    ModuleParameter.type_ == param_type,
                    ModuleParameter.value.in_(param_values),
                )
            )
            for param_type, param_values in parameters.items()
        ]
    else:
        filters = [true()]

    if q != "":
        filters.append(
            Module.id.in_(
                select(text("id"))
                .select_from(text("modules_fts"))
                .where(text("modules_fts MATCH :q"))
            ).params(q=escaped_query)
        )
        repo_url_query: False_ | BinaryExpression[bool] = Module.id.in_(
            select(Module.id)
            .join(Repository, Module.repo_id == Repository.id)
            .where(Repository.url.like("%:q%"))
            .params(q=q)
        )
    else:
        repo_url_query = false()

    total_count = (
        await session.execute(
            select(func.count())
            .select_from(Module)
            .where(
                or_(and_(*filters), repo_url_query),
                Module.deleted_at.is_(None),
                has_permission_for(user, Module),
            )
        )
    ).scalar_one()
    total_pages = (total_count + limit - 1) // limit

    modules = (
        (
            await session.execute(
                select(Module)
                .where(
                    or_(and_(*filters), repo_url_query),
                    Module.deleted_at.is_(None),
                    has_permission_for(user, Module),
                )
                # `random_with_seed_from_id` is user-defined function, defined in `app/db/connection.py``
                # It is used to generate a random number from the rowid of the module
                # and the seed given as parameter.
                # it orders the rows randomly but it's guaranteed that the order is the same
                # for a given seed.
                .order_by(func.random_with_seed_from_id(seed, text("rowid")))
                .offset(page * limit)
                .limit(limit)
                .options(selectinload(Module.user), selectinload(Module.parameters))
            )
        )
        .scalars()
        .all()
    )
    return modules, total_pages


async def get_all_parameters(session: AsyncSession) -> dict[str, Any]:
    """
    Group by the parameter type and return a dict
    where the key is the type and the value is a list of all the parameters of that type
    """
    parameters = (
        await session.execute(
            select(
                ModuleParameter.type_,
                func.group_concat(ModuleParameter.value.distinct()),
            )
            .filter(ModuleParameter.deleted_at.is_(None))
            .group_by(ModuleParameter.type_)
            .order_by(ModuleParameter.type_)
        )
    ).all()

    return {type_: values.split(",") for type_, values in parameters}


async def delete_module(
    username: str,
    repo_name: str,
    module_name: str,
    user: User | None,
    session: AsyncSession,
) -> None:
    module = await get_module_by_name(username, repo_name, module_name, user, session)
    module.deleted_at = datetime.now()
    await session.commit()


async def get_all_slugs(user: User | None, session: AsyncSession) -> Sequence[str]:
    """Get all the slugs of the modules."""
    return (
        (
            await session.execute(
                select(
                    func.concat(
                        User.displayname,
                        "/",
                        Repository.name,
                        "/",
                        Module.name,
                    )
                )
                .select_from(Module)
                .join(User, User.id == Module.user_id)
                .join(Repository, Repository.id == Module.repo_id)
                .where(
                    Module.deleted_at.is_(None),
                    Repository.deleted_at.is_(None),
                    User.disabled.is_(False),
                    # has_permission_for(user, Module),
                )
            )
        )
        .scalars()
        .all()
    )
