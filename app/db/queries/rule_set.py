"""
Queries for the `rule_sets` table.
SPDX-License-Identifier: EUPL-1.2
"""

from datetime import datetime
from typing import Sequence
from uuid import UUID

from sqlalchemy import func, select, text
from sqlalchemy.ext.asyncio import AsyncSession

from app.config import CONFIG
from app.db.models.rule_set import RuleSet, RuleSetNotFound
from app.db.models.user import User
from app.db.queries.common import has_permission_for


async def get_repo_rule_sets(
    id: UUID, user: User | None, session: AsyncSession
) -> Sequence[RuleSet]:
    return (
        (
            await session.execute(
                select(RuleSet).where(
                    RuleSet.repo_id == str(id),
                    has_permission_for(user, RuleSet),
                    RuleSet.deleted_at.is_(None),
                )
            )
        )
        .scalars()
        .all()
    )


async def pagination(
    user: User | None, page: int, limit: int, seed: int, session: AsyncSession
) -> Sequence[RuleSet]:
    """Random consistent pagination.
    \nReturn rule_sets in a random order
    but the order is constant for a given seed."""
    if page < 0 or limit <= 0:
        return []

    return (
        (
            await session.execute(
                select(RuleSet)
                .where(
                    RuleSet.deleted_at.is_(None),
                    has_permission_for(user, RuleSet),
                )
                # `random_with_seed_from_id` is user-defined function, defined in `app/db/connection.py`
                # It is used to generate a random number from the rowid of the module
                # and the seed given as parameter.
                # it orders the rows randomly but it's guaranteed that the order is the same
                # for a given seed.
                .order_by(func.random_with_seed_from_id(seed, text("rowid")))
                .offset(page * limit)
                .limit(limit)
            )
        )
        .scalars()
        .all()
    )


async def get_rule_set_by_id(
    rule_set_id: UUID, user: User | None, session: AsyncSession
) -> RuleSet:
    rule_set = (
        await session.execute(
            select(RuleSet).where(
                RuleSet.id == str(rule_set_id),
                RuleSet.deleted_at.is_(None),
                has_permission_for(user, RuleSet),
            )
        )
    ).scalar_one_or_none()

    if rule_set is None:
        raise RuleSetNotFound(f"Could not find rules with id `{rule_set_id}`.")
    return rule_set


async def get_rule_set_by_name(
    username: str, name: str, user: User | None, session: AsyncSession
) -> RuleSet:
    rule_set = (
        await session.execute(
            select(RuleSet).where(
                RuleSet.name == name,
                RuleSet.deleted_at.is_(None),
                RuleSet.user_id
                == select(User.id)
                .where(
                    User.displayname == username,
                    User.disabled.is_(False),
                    has_permission_for(user, RuleSet),
                )
                .scalar_subquery(),
            )
        )
    ).scalar_one_or_none()

    if rule_set is None:
        raise RuleSetNotFound(
            f"Could not find rules with name `{name}` for user `{username}`."
        )
    return rule_set


async def full_text_search(
    user: User | None, q: str, session: AsyncSession  # pylint: disable=invalid-name
) -> Sequence[RuleSet]:
    if q == "":
        return []

    escaped_query = '"' + q.replace('"', '""') + '"'
    rule_sets = (
        (
            await session.execute(
                select(RuleSet)
                .where(
                    RuleSet.id.in_(
                        select(text("id"))
                        .select_from(text("rule_sets_fts"))
                        .where(text("rule_sets_fts MATCH :q"))
                    ),
                    RuleSet.deleted_at.is_(None),
                    has_permission_for(user, RuleSet),
                )
                .params(q=escaped_query)
                .limit(CONFIG.results_per_page)
            )
        )
        .scalars()
        .all()
    )
    return rule_sets


async def delete_rule_set(
    username: str, module_name: str, user: User | None, session: AsyncSession
) -> None:
    rules = await get_rule_set_by_name(username, module_name, user, session)
    rules.deleted_at = datetime.now()
    await session.commit()
