"""
Queries for the `categories` table.
SPDX-License-Identifier: EUPL-1.2
"""

from typing import Final

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.db import connection, models

DEFAULT_CATEGORIES: Final = [
    dict(name="power", description="Power supply"),
    dict(name="mcu", description="Microcontroller"),
    dict(name="connector", description="Connector"),
    dict(name="other", description="default category"),
]


async def create_categories() -> None:
    """On startup add the categories in the database."""
    session = connection.SessionLocal()

    categories_in_db = (
        (await session.execute(select(models.Category.name).distinct())).scalars().all()
    )

    new_categories = [
        models.Category(**category)
        for category in DEFAULT_CATEGORIES
        if category["name"] not in categories_in_db
    ]

    if len(new_categories) > 0:
        session.add_all(new_categories)

    await session.commit()
    await session.close()


async def get_category_by_name(
    name: str | None, session: AsyncSession
) -> models.Category:
    """Get a category by name or return the default category."""
    if name is None or name not in [
        category["name"] for category in DEFAULT_CATEGORIES
    ]:
        name = "other"

    name = name.lower()

    return (
        await session.execute(
            select(models.Category).where(models.Category.name == name)
        )
    ).scalar_one()
