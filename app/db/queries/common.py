"""
SPDX-License-Identifier: EUPL-1.2
"""

from typing import TYPE_CHECKING, Type

from sqlalchemy import BinaryExpression, ColumnElement, func, or_

from app.db.models.module import Module
from app.db.models.rule_set import RuleSet
from app.db.models.user import User


def has_permission_for(
    user: User | None, model: Type[Module | RuleSet]
) -> BinaryExpression[bool] | ColumnElement[bool]:
    """The user is authorized for operations over a model if it is public, the user isthe owner, or an admin."""

    if TYPE_CHECKING:
        # for mypy, pyright gets it right!
        assert hasattr(model.private, "is_")  # pragma: no cover
        assert hasattr(model.user_id, "is_")  # pragma: no cover

    if user is None:
        # Only return public models
        return model.private.is_(False)
    else:
        return or_(
            # return public models
            model.private.is_(False),
            # plus user's private models
            model.user_id.is_(user.id),
            # or any models for admin
            func.lower("admin").in_(user.roles),
        )
