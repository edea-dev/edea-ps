"""
SPDX-License-Identifier: EUPL-1.2
"""

import contextlib
import random
from typing import Any, AsyncGenerator

from alembic.command import upgrade
from alembic.config import Config
from alembic.migration import MigrationContext
from alembic.script import ScriptDirectory
from sqlalchemy import event
from sqlalchemy.dialects.sqlite.aiosqlite import AsyncAdapt_aiosqlite_connection
from sqlalchemy.exc import MissingGreenlet
from sqlalchemy.ext.asyncio import (
    AsyncConnection,
    AsyncSession,
    async_sessionmaker,
    create_async_engine,
)
from sqlalchemy.orm import DeclarativeBase

from app.config import CONFIG

engine = create_async_engine(CONFIG.db_url)
SessionLocal = async_sessionmaker(engine, expire_on_commit=False)


class Base(DeclarativeBase):
    """Base class for models."""


async def get_db_session() -> AsyncGenerator[AsyncSession, None]:
    """Get a database session."""
    session = SessionLocal()
    try:
        yield session
    finally:
        await session.close()


@event.listens_for(engine.sync_engine, "connect")
def add_rand_with_seed(dbapi_conn: AsyncAdapt_aiosqlite_connection, _: Any) -> None:
    """
    Define a user-defined in SQLite that
    returns a random number between -9223372036854775808  and 9223372036854775807
    the seed is the sum of the rowid and the seed passed as parameter
    """

    def random_with_seed_from_id(
        seed: int, id_: int
    ) -> int:  # pragma: no cover # this is only used in SQLite
        """
        Return a random number between -9223372036854775808  and 9223372036854775807.
        """
        random.seed(seed + id_)
        return random.randint(
            -9223372036854775808, 9223372036854775807
        )  # nosec # this isn't used for cryptography

    dbapi_conn.create_function("random_with_seed_from_id", 2, random_with_seed_from_id)


async def apply_migrations(conn: AsyncConnection) -> None:
    """
    Checks if there are pending migrations and performs them if necessary
    """
    is_latest = False
    config = Config()
    config.set_main_option("script_location", "app/migrations")
    config.set_main_option("sqlalchemy.url", CONFIG.alembic_db_url)
    directory = ScriptDirectory.from_config(config)
    context = await conn.run_sync(MigrationContext.configure)
    with contextlib.suppress(MissingGreenlet):
        is_latest = set(context.get_current_heads()) == set(directory.get_heads())
    if not is_latest:
        print("Applying migrations")
        upgrade(config, "head")
