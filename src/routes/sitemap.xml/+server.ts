import * as sitemap from 'super-sitemap';
import type { RequestHandler } from '@sveltejs/kit';
import { ENV } from '$ENV';
import { getSlugs } from '$client';
import { unbox } from '$client/edea';

export const prerender = false;

export const GET: RequestHandler = async () => {
	const slugs = await getSlugs().then(unbox);
	const usernames = new Set<string>();
	const repos = new Set<string>();
	const modules = new Set<string>();
	const explodedSlugs = slugs.map((slug) => slug.split('/').filter(Boolean));

	explodedSlugs.forEach((parts) => {
		const [username, repo, ...rest] = parts;
		usernames.add(username);
		repos.add(`${username}/${repo}`);
		modules.add(`${username}/${repo}/${encodeURIComponent(rest.join('/'))}`);
	});

	return await sitemap.response({
		origin: ENV.frontendUrl,
		paramValues: {
			'/[username]': Array.from<string>(usernames),
			'/[username]/[repo]': Array.from(repos),
			'/[username]/[repo]/[module]': Array.from(modules),
		},
	});
};
