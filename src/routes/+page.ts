import { getAllParameters, searchModule } from '$client/services.gen';
import { ENV } from '$ENV';
import JsonURL from '@jsonurl/jsonurl';
import type { PageLoad } from './$types';
import { unbox, client } from '$client/edea';

export const load: PageLoad = async ({ url, fetch }) => {
	const allParameters = await getAllParameters({ fetch, client }).then(unbox);

	const query = url.searchParams.get('q') ?? '';
	const parameters = JsonURL.parse(url.searchParams.get('parameters') ?? '()', { AQF: true });
	const page = parseInt(url.searchParams.get('page') ?? '1', 10) - 1;
	const search = await searchModule({
		client,
		credentials: 'include',
		fetch,
		body: {
			limit: ENV.resultsPerPage,
			page,
			query,
			parameters: !Array.isArray(parameters) ? [] : parameters,
		},
	}).then(unbox);

	return {
		seachableParameters: allParameters,
		totalPages: search.total_pages,
		modules: search.hits,
	};
};
