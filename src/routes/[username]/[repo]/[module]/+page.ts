import type { PageLoad } from './$types';
import { getCommits, getModuleByName, getReadme, getViolations } from '$client';
import { unbox, client } from '$client/edea';

export const load: PageLoad = async ({ params, fetch }) => {
	const { username, module: module_name, repo: repo_name } = params;
	const path = { username, repo_name, module_name };
	const [module, readme, commits, violations] = await Promise.all([
		getModuleByName({ client, fetch, path, credentials: 'include' }).then(unbox),
		getReadme({ client, fetch, path, credentials: 'include' }).then(unbox),
		getCommits({ client, fetch, path, credentials: 'include' }).then(unbox),
		getViolations({ client, fetch, path, credentials: 'include' }).then(unbox),
	]);

	return {
		module,
		readme,
		commits,
		violations,
	};
};
