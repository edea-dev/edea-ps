import type { PageLoad } from './$types';
import { getRepoModules, type ModuleSchema } from '$client';
import { unbox, client } from '$client/edea';

export const load: PageLoad = async ({ params, fetch }) => {
	const modulesAndRules = await getRepoModules({
		client,
		credentials: 'include',
		fetch,
		path: {
			username: params.username,
			repo_name: params.repo,
		},
	}).then(unbox);
	return {
		modules: modulesAndRules.filter((m): m is ModuleSchema => 'parameters' in m),
	};
};
