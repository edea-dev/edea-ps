import { getUserModules } from '$client';
import type { PageLoad } from './$types';
import { unbox, client } from '$client/edea';

export const load: PageLoad = async ({ params, fetch }) => {
	const { username } = params;
	const modules = await getUserModules({
		client,
		credentials: 'include',
		fetch,
		path: { username },
	}).then(unbox);
	return { modules };
};
