import { ENV } from '$ENV';

export const prerender = true;

export async function GET(): Promise<Response> {
	// prettier-ignore
	const body = [
        'User-agent: *',
        'Allow: /',
        '',
        `Sitemap: ${ENV.frontendUrl}/sitemap.xml`
    ].join('\n').trim();

	const headers = {
		'Content-Type': 'text/plain',
	};

	return new Response(body, { headers });
}
