import { client } from '$client/edea.js';
import { getSelf } from '$client';
import type { LayoutLoad } from './$types';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const ssr = false;

export const load: LayoutLoad = async ({ fetch }) => {
	const res = await getSelf({ fetch, client, credentials: 'include', mode: 'cors' });
	if (res.response.status === 200 && res.data) {
		return { user: res.data };
	}
	return {
		user: undefined,
	};
};
