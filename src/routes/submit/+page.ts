import { client } from '$client/edea.js';
import { ENV } from '$ENV';
import { getSelf } from '$client';
import { redirect } from '@sveltejs/kit';
import type { PageLoad } from './$types';

export const load: PageLoad = async ({ fetch, url }) => {
	const user = await getSelf({ fetch, client, credentials: 'include', mode: 'cors' });
	if (user.response.status !== 200) {
		redirect(307, `${ENV.backendUrl}/v1/user/login?came_from=${encodeURIComponent(url.href)}`);
	}
};
