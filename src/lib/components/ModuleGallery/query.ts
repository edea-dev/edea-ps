import { writable } from 'svelte/store';

import type { SearchParameter } from '$client';

export const query = writable('');
export const parameterFilter = writable('');
export const parametericQuery = writable<Array<SearchParameter>>([]);
