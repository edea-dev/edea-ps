import { writable, type Writable } from 'svelte/store';
import type { UserSchema } from '$client/types.gen';

export const userStore: Writable<UserSchema> = writable();
