interface ImportMetaEnv {
	resultsPerPage: number;
	backendUrl: string;
	frontendUrl: string;
	host: string;
}

export const ENV: ImportMetaEnv = {
	resultsPerPage: parseInt(import.meta.env.VITE_RESULTS_PER_PAGE, 10),
	frontendUrl: import.meta.env.VITE_FRONTEND_URL,
	backendUrl: import.meta.env.VITE_BACKEND_URL,
	host: new URL(import.meta.env.VITE_FRONTEND_URL ?? 'https://edea.test').hostname,
};
