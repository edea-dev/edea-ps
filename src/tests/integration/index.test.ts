import { test, expect } from '@playwright/test';

test('First integration test', async ({ page }) => {
	await page.goto('/');
	await expect(page.getByText('Add Your Module')).toBeVisible();
});
