import { it, expect } from 'vitest';
import { render, screen } from '@testing-library/svelte';
import Footer from '$lib/components/Footer.svelte';

it('Footer should have repo link', () => {
	render(Footer);
	expect(screen.getByTitle("EDeA's protal server GitLab repository")).toBeInTheDocument();
});
