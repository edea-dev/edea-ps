import type { RequestResult } from "@hey-api/client-fetch";
import { type NumericRange, error } from "@sveltejs/kit";
import { client as defaultClient } from './index'
import { ENV } from "$ENV";

export function unbox<X, Y>(requestResult: Awaited<RequestResult<X, Y>>): X {
    if (requestResult.error || !requestResult.data) {
        const status = requestResult.response.status as NumericRange<400, 599>
        error(status, requestResult.response.statusText);
    }
    return requestResult.data;
}

defaultClient.setConfig({
    baseUrl: ENV.backendUrl,
})

export const client = defaultClient;
