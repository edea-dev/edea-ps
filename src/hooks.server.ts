export const handle: import('@sveltejs/kit').Handle = async ({ event, resolve }) => {
	const response = await resolve(event, {
		// include all headers from the original response
		filterSerializedResponseHeaders: Boolean,
	});
	return response;
};
