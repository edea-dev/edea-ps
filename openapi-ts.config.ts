import { defineConfig } from '@hey-api/openapi-ts';

export default defineConfig({
    client: '@hey-api/client-fetch',
    input: 'openapi.json',
    output: {
        lint: 'eslint',
        format: 'prettier',
        path: 'src/client',
    },
    schemas: {
        type: 'json'
    },
});