services:
  devcontainer:
    env_file: ../.env
    image: registry.gitlab.com/edea-dev/ci-runner:latest
    volumes:
      - ../..:/workspace:cached
      - /edea/db:/db
      - /edea/storage:/storage
    command: sleep infinity
    networks:
      - caddy_net

  backend:
    env_file: ../.env
    environment:
      - CADDY_LOCAL_CERT_PATH=/caddy-data/caddy/pki/authorities/local/root.crt
    build:
      context: ..
      dockerfile: Dockerfile.app
    depends_on:
      - worker
    networks:
      - caddy_net
    volumes:
      - /edea/db:/db
      - /edea/storage:/storage
      - /etc/hosts:/etc/hosts:ro
      - /caddy/data:/caddy-data:ro
      - ../app:/app/app
    restart: unless-stopped
    command: 'uvicorn app.main:app --host 0.0.0.0 --port 8080 --reload'

  worker:
    env_file: ../.env
    environment:
      - CADDY_LOCAL_CERT_PATH=/caddy-data/caddy/pki/authorities/local/root.crt
    build:
      context: ..
      dockerfile: Dockerfile.app
    depends_on:
      - redis
    networks:
      - caddy_net
    volumes:
      - /etc/hosts:/etc/hosts:ro
      - /caddy/data:/caddy-data
      - /edea/db2:/db
      - /edea/storage:/storage
    restart: unless-stopped
    command: 'taskiq worker app.services.tasks:broker --workers 1'

  frontend:
    env_file: ../.env
    environment:
      - NODE_EXTRA_CA_CERTS=/caddy-data/caddy/pki/authorities/local/root.crt
    build:
      context: ..
      dockerfile: Dockerfile.src
      target: dev
    depends_on:
      - backend
    networks:
      - caddy_net
    volumes:
      - ../src:/app/src
      - ../package.json:/app/package.json
      - ../svelte.config.js:/app/svelte.config.js
      - /etc/hosts:/etc/hosts:ro
      - /caddy/data:/caddy-data
    restart: unless-stopped

  caddy:
    env_file: ../.env
    image: caddy
    volumes:
      - ../caddy/:/etc/caddy
      - /etc/hosts:/etc/hosts:ro
      - /caddy/data:/data
      - /caddy/config:/config
    ports:
      - '80:80'
      - '443:443'
    networks:
      - caddy_net

    restart: unless-stopped

  redis:
    env_file: ../.env
    image: redis:7.4
    command: redis-server --requirepass 123456
    networks:
      - caddy_net
    restart: unless-stopped

  dex:
    env_file: ../.env
    image: dexidp/dex:latest-distroless
    ports:
      - '5555:5555'
      - '5556:5556'
    secrets:
      - source: dex-config
        target: dex-config.yaml
    networks:
      - caddy_net
    restart: always
    command: 'dex serve /run/secrets/dex-config.yaml'

  offen:
    image: offen/offen:development
    environment:
      OFFEN_SERVER_REVERSEPROXY: 1
      OFFEN_DATABASE_DIALECT: sqlite3
    networks:
      - caddy_net
    entrypoint: >
      sh -c 'offen setup -force -forceid e2be23fb-399b-49fb-83f7-fdcf1949b960 -email email@example.com -name example -password 12345678 && offen serve'

secrets:
  dex-config:
    file: ../dex-config.yaml

networks:
  caddy_net:
