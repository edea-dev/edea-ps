"""
Export and modify the openapi spec from the FastAPI app.
The output is used by https://github.com/ferdikoomen/openapi-typescript-codegen to generate the typescript client.
See the README.md for more information.
"""

import json
from pathlib import Path

from uvicorn.importer import import_from_string

file_path = Path("./openapi.json")

if __name__ == "__main__":
    print("importing app from main:app")
    app = import_from_string("app.main:app")
    openapi = app.openapi()
    version = openapi.get("openapi", "unknown version")

    print(f"writing openapi spec v{version}")
    for path_data in openapi["paths"].values():
        for operation in path_data.values():
            operaion_id = operation["summary"].title().replace(" ", "")
            operation["operationId"] = operaion_id[0].lower() + operaion_id[1:]

            operation["summary"]

    health_content = openapi["paths"]["/health"]["get"]["responses"]["200"]["content"]
    openapi["paths"]["/health"]["get"]["responses"]["200"]["content"][
        "application/health+json"
    ] = health_content.pop("application/json")

    openapi["servers"] = [
        {
            "url": "https://api.edea.com",
            "description": "Producation EDeA portal API.",
            "x-internal": False,
        },
    ]

    file_path.write_text(json.dumps(openapi, indent=2))

    print(f"spec written to {file_path.absolute()}")
