# EDeA Portal Server

## Dependencies

- redis: `docker run -p 6379:6379 -it redis/redis-stack:latest` or you can use [redis-cloud](https://redis.com/try-free/)
- authentication-server: to get the `JWKS_URL` you can use [Auth0](https://auth0.com/)
- [libvips](https://www.libvips.org/) for generating thumbnails

## Developing

### Install python and node dependencies

```sh
poetry install
poetry run nodeenv -p
poetry run npm install
```

### Update the environment variables in `.env.example`

```sh
cp .env.example .env
```

`JWKS_URL`, `GITLAB_PRIVATE_TOKEN`, and `REDIS_BROKER_URL` must be updated, the server won't run without that.

### Run the dev servers

```sh
poetry run taskiq worker app.services.tasks:broker        # run the taskiq worker
poetry run taskiq scheduler app.services.tasks:scheduler  # run the taskiq scheduler
poetry run uvicorn app.main:app --reload                  # backend on http://localhost:8000
poetry run npm run dev --open                             # frontend on http://localhost:5173
```

or using poe

```sh
poe worker
poe scheduler
poe backend
poe frontend
```

#### Generating or updating the frontend client

```sh
poe generate-client
```

## Structure (partial)

```
.
├── alembic               - Database migration with Alembic.
├── app                   - Backend.
├── src                   - Frontend.
└── static                - Frontend static files like images, CSS, or JavaScript.
```
